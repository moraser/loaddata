package com.cerounocenter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cerounocenter.entidades.CntCuedoc;
import com.cerounocenter.entidades.CntCuedocPK;
import com.cerounocenter.entidades.CntPuc;
import com.cerounocenter.entidades.CntTipodoc;
import com.cerounocenter.entidades.GenCcosto;
import com.cerounocenter.entidades.GenClasif1;
import com.cerounocenter.entidades.GenClasif2;
import com.cerounocenter.entidades.GenClasif3;
import com.cerounocenter.entidades.GenMonedas;
import com.cerounocenter.entidades.GenSucursal;
import com.cerounocenter.entidades.GenTerceros;
import com.cerounocenter.service.CntCuedocService;

@RestController
@RequestMapping("/load")
public class ControllerLoad {
	
	@Autowired
	@Qualifier("cntCuedocService")
	private CntCuedocService cntCuedocService;
	
	@GetMapping("/init")
	public String inicia() {
		List<SaldosNegativos> listado = null;
		String pathFile = "D:\\tmp\\original1305.csv";
		try {
			listado = leerArhivoCSV(pathFile);
		} catch (Exception e) {
			System.out.println("Error leyendo archivo...");
		}
		if(listado != null) {
			System.out.println("Archivo leido correctamente");
			System.out.println("Tamaño >> "+listado.size());
			procesarArchivo(listado);
			return "Archivo leido correctamente";
		}else {
			System.out.println("Archivo no pudo ser leido");
			return "Archivo no pudo ser leido";
		}
	}

	@SuppressWarnings({ "unused", "resource" })
	public List<SaldosNegativos> leerArhivoCSV(String archCSV) throws IOException {
		System.out.println("Leyendo archivo");
		try {
			// String archCSV = "D:\\tmp\\original1305.csv";
			// CSVReader reader = new CSVReader(new FileReader(archCSV));
			List<SaldosNegativos> list = new ArrayList<>();
			String[] nextLine;
			String cvsSplitBy = ";";
			BufferedReader br = null;
			String line = "";
			br = new BufferedReader(new FileReader(archCSV));
			while ((line = br.readLine()) != null) {
				SaldosNegativos sn = new SaldosNegativos();
				// use comma as separator
				String[] string = line.split(cvsSplitBy);
				sn.setAno_doc(string[0]);
				sn.setPer_doc(string[1]);
				sn.setTip_doc(string[2]);
				sn.setNum_doc(string[3]);
				sn.setReg_doc(string[4]);
				sn.setCod_cta(string[5]);
				sn.setTrans(string[6]);
				sn.setCod_suc(string[7]);
				sn.setCod_cco(string[8]);
				sn.setCod_ter(string[9]);
				sn.setNum_che(string[10]);
				sn.setBas_mov(string[11]);
				sn.setDes_mov(string[12]);
				sn.setDeb_mov(string[13]);
				sn.setCre_mov(string[14]);
				sn.setTip_mov(string[15]);
				sn.setFch_mov(string[16]);
				sn.setCod_cl1(string[17]);
				sn.setCod_cl2(string[18]);
				sn.setCod_cl3(string[19]);
				sn.setCod_dif(string[20]);
				sn.setDiferencia(string[21]);
				sn.setDex_mov(string[22]);
				sn.setCex_mov(string[23]);
				sn.setMarca(string[24]);
				sn.setInd_mr(string[25]);
				sn.setFec_tas(string[26]);
				sn.setTasa(string[27]);
				sn.setSub_tip(string[28]);
				sn.setFec_cte(string[29]);
				sn.setIDL_num(string[30]);
				sn.setCod_plt(string[31]);
				list.add(sn);
			}
			// reader.close();
			return list;
		} catch (Exception e) {
			return null;
		}
	}

	public void procesarArchivo(List<SaldosNegativos> listado) {
		int can = 0;
		for (SaldosNegativos saldo : listado) {
			if (!saldo.getAno_doc().contains("ano_doc")) {
				CntCuedocPK cuedocPK = new CntCuedocPK();
				cuedocPK.setAnoDoc(saldo.getAno_doc());
				cuedocPK.setNumDoc(saldo.getNum_doc());
				cuedocPK.setPerDoc(saldo.getPer_doc());
				cuedocPK.setRegDoc(Integer.parseInt(saldo.getReg_doc()));
				cuedocPK.setTipDoc(saldo.getTip_doc());
				CntCuedoc cuedoc = new CntCuedoc();
				cuedoc.setCntCuedocPK(cuedocPK);
				cuedoc.setBasMov(new BigDecimal(saldo.getBas_mov().replace(",", ".")));
				cuedoc.setCexMov(new BigDecimal(saldo.getCex_mov().replace(",", ".")));
				cuedoc.setCntTipodoc(new CntTipodoc(saldo.getTip_doc()));
				cuedoc.setCodCco(new GenCcosto(saldo.getCod_cco()));
				cuedoc.setCodCl1(new GenClasif1(saldo.getCod_cl1()));
				cuedoc.setCodCl2(new GenClasif2(saldo.getCod_cl2()));
				cuedoc.setCodCl3(new GenClasif3(saldo.getCod_cl3()));
				cuedoc.setCodCta(new CntPuc(saldo.getCod_cta()));
				cuedoc.setCodDif(saldo.getCod_dif());
				cuedoc.setCodPlt(saldo.getCod_plt());
				cuedoc.setCodSuc(new GenSucursal(saldo.getCod_suc()));
				cuedoc.setCodTer(new GenTerceros(saldo.getCod_ter()));
				cuedoc.setCreMov(new BigDecimal(saldo.getCre_mov().replace(",", ".")));
				cuedoc.setDebMov(new BigDecimal(saldo.getDeb_mov().replace(",", ".")));
				cuedoc.setDesMov(saldo.getDes_mov());
				cuedoc.setDexMov(new BigDecimal(saldo.getDex_mov().replace(",", ".")));
				cuedoc.setDiferencia(new BigDecimal(saldo.getDiferencia().replace(",", ".")));
				cuedoc.setFchMov(stringToDate(saldo.getFch_mov()));
				cuedoc.setFecCte(stringToDate(saldo.getFec_cte()));
				cuedoc.setFecTas(stringToDate(saldo.getFec_tas()));
				cuedoc.setIDLnum(new BigInteger(saldo.getIDL_num()));
				cuedoc.setIndMr(new GenMonedas(saldo.getInd_mr()));
				cuedoc.setMarca(saldo.getMarca().charAt(0));
				cuedoc.setNumChe(saldo.getNum_che());
				cuedoc.setSubTip(saldo.getSub_tip());
				cuedoc.setTasa(new BigDecimal(saldo.getTasa().replace(",", ".")));
				cuedoc.setTipMov(saldo.getTip_mov().charAt(0));
				cuedoc.setTrans(saldo.getTrans());
				System.out.println("Registradno Objeto >> " + cuedoc.getCntCuedocPK().toString());
				CntCuedoc resp = cntCuedocService.registrar(cuedoc);
				if(resp != null) {
					can++;
					System.out.println("Registro realizado satisfactoriamente.");
				}
			}
		}
		System.out.println("Total registrados >> "+can);
	}

	@SuppressWarnings("unused")
	private Date stringToDate(String fecha) {
		try {
			Date date1 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(fecha);
			return date1;
		} catch (Exception e) {
			return new Date();
		}

	}

}
