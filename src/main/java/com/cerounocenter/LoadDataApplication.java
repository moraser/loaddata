package com.cerounocenter;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoadDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoadDataApplication.class, args);
	}

	/*
	public void run(String... args) throws Exception {
		String pathFile = null;
		if (args.length > 0) {
            pathFile = args[0].toString();
        } else {
            pathFile = "D:\\tmp\\original1305.csv";
        }        
		ControllerLoad load = new ControllerLoad();
		List<SaldosNegativos> listado = load.leerArhivoCSV(pathFile);
		if(listado != null) {
			System.out.println("Archivo leido correctamente");
			System.out.println("Tamaño >> "+listado.size());
			load.procesarArchivo(listado);
		}else {
			System.out.println("Archivo no pudo ser leido");
		}
		ControllerMain main = new ControllerMain();
		main.test();
		System.exit(0);
	}*/

}

