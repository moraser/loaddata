package com.cerounocenter;



public class SaldosNegativos {

	private String ano_doc;
	private String per_doc;
	private String tip_doc;
	private String num_doc;
	private String reg_doc;
	private String cod_cta;
	private String trans;
	private String cod_suc;
	private String cod_cco;
	private String cod_ter;
	private String num_che;
	private String bas_mov;
	private String des_mov;
	private String deb_mov;
	private String cre_mov;
	private String tip_mov;
	private String fch_mov;
	private String cod_cl1;
	private String cod_cl2;
	private String cod_cl3;
	private String cod_dif;
	private String diferencia;
	private String dex_mov;
	private String cex_mov;
	private String marca;
	private String ind_mr;
	private String fec_tas;
	private String tasa;
	private String sub_tip;
	private String fec_cte;
	private String IDL_num;
	private String cod_plt;
	public String getAno_doc() {
		return ano_doc;
	}
	public void setAno_doc(String ano_doc) {
		this.ano_doc = ano_doc;
	}
	public String getPer_doc() {
		return per_doc;
	}
	public void setPer_doc(String per_doc) {
		this.per_doc = per_doc;
	}
	public String getTip_doc() {
		return tip_doc;
	}
	public void setTip_doc(String tip_doc) {
		this.tip_doc = tip_doc;
	}
	public String getNum_doc() {
		return num_doc;
	}
	public void setNum_doc(String num_doc) {
		this.num_doc = num_doc;
	}
	public String getReg_doc() {
		return reg_doc;
	}
	public void setReg_doc(String reg_doc) {
		this.reg_doc = reg_doc;
	}
	public String getCod_cta() {
		return cod_cta;
	}
	public void setCod_cta(String cod_cta) {
		this.cod_cta = cod_cta;
	}
	public String getTrans() {
		return trans;
	}
	public void setTrans(String trans) {
		this.trans = trans;
	}
	public String getCod_suc() {
		return cod_suc;
	}
	public void setCod_suc(String cod_suc) {
		this.cod_suc = cod_suc;
	}
	public String getCod_cco() {
		return cod_cco;
	}
	public void setCod_cco(String cod_cco) {
		this.cod_cco = cod_cco;
	}
	public String getCod_ter() {
		return cod_ter;
	}
	public void setCod_ter(String cod_ter) {
		this.cod_ter = cod_ter;
	}
	public String getNum_che() {
		return num_che;
	}
	public void setNum_che(String num_che) {
		this.num_che = num_che;
	}
	public String getBas_mov() {
		return bas_mov;
	}
	public void setBas_mov(String bas_mov) {
		this.bas_mov = bas_mov;
	}
	public String getDes_mov() {
		return des_mov;
	}
	public void setDes_mov(String des_mov) {
		this.des_mov = des_mov;
	}
	public String getDeb_mov() {
		return deb_mov;
	}
	public void setDeb_mov(String deb_mov) {
		this.deb_mov = deb_mov;
	}
	public String getCre_mov() {
		return cre_mov;
	}
	public void setCre_mov(String cre_mov) {
		this.cre_mov = cre_mov;
	}
	public String getTip_mov() {
		return tip_mov;
	}
	public void setTip_mov(String tip_mov) {
		this.tip_mov = tip_mov;
	}
	public String getFch_mov() {
		return fch_mov;
	}
	public void setFch_mov(String fch_mov) {
		this.fch_mov = fch_mov;
	}
	public String getCod_cl1() {
		return cod_cl1;
	}
	public void setCod_cl1(String cod_cl1) {
		this.cod_cl1 = cod_cl1;
	}
	public String getCod_cl2() {
		return cod_cl2;
	}
	public void setCod_cl2(String cod_cl2) {
		this.cod_cl2 = cod_cl2;
	}
	public String getCod_cl3() {
		return cod_cl3;
	}
	public void setCod_cl3(String cod_cl3) {
		this.cod_cl3 = cod_cl3;
	}
	public String getCod_dif() {
		return cod_dif;
	}
	public void setCod_dif(String cod_dif) {
		this.cod_dif = cod_dif;
	}
	public String getDiferencia() {
		return diferencia;
	}
	public void setDiferencia(String diferencia) {
		this.diferencia = diferencia;
	}
	public String getDex_mov() {
		return dex_mov;
	}
	public void setDex_mov(String dex_mov) {
		this.dex_mov = dex_mov;
	}
	public String getCex_mov() {
		return cex_mov;
	}
	public void setCex_mov(String cex_mov) {
		this.cex_mov = cex_mov;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getInd_mr() {
		return ind_mr;
	}
	public void setInd_mr(String ind_mr) {
		this.ind_mr = ind_mr;
	}
	public String getFec_tas() {
		return fec_tas;
	}
	public void setFec_tas(String fec_tas) {
		this.fec_tas = fec_tas;
	}
	public String getTasa() {
		return tasa;
	}
	public void setTasa(String tasa) {
		this.tasa = tasa;
	}
	public String getSub_tip() {
		return sub_tip;
	}
	public void setSub_tip(String sub_tip) {
		this.sub_tip = sub_tip;
	}
	public String getFec_cte() {
		return fec_cte;
	}
	public void setFec_cte(String fec_cte) {
		this.fec_cte = fec_cte;
	}
	public String getIDL_num() {
		return IDL_num;
	}
	public void setIDL_num(String iDL_num) {
		IDL_num = iDL_num;
	}
	public String getCod_plt() {
		return cod_plt;
	}
	public void setCod_plt(String cod_plt) {
		this.cod_plt = cod_plt;
	}
	
	
}
