package com.cerounocenter.repository;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.cerounocenter.entidades.CntCuedoc;
import com.cerounocenter.entidades.CntCuedocPK;

@Repository("cntCuedocServiceRepository")
public interface CntCuedocServiceRepository extends JpaRepository<CntCuedoc, Serializable> {
	
	List<CntCuedoc> findByCntCuedocPK(CntCuedocPK pk);
	
	@Query("SELECT c FROM CntCuedoc c WHERE c.cntCuedocPK.anoDoc = :anoDoc AND c.cntCuedocPK.perDoc = :perDoc AND c.cntCuedocPK.tipDoc = :tipDoc")
	List<CntCuedoc> findByFilter(@Param("anoDoc") String anoDoc, 
			@Param("perDoc") String perDoc, @Param("tipDoc") String tipDoc);

}
