package com.cerounocenter.service;

import java.util.List;

import com.cerounocenter.entidades.CntCuedoc;

public interface CntCuedocService {
	
	List<CntCuedoc> findAllByAnoMesTipDoc(String anoDoc, String mesDoc, String tipDoc);
	
	List<CntCuedoc> findAllFilter(String anoDoc, String mesDoc, String tipDoc);
	
	CntCuedoc registrar(CntCuedoc cuedoc);

}
