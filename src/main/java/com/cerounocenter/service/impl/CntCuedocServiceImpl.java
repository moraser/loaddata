package com.cerounocenter.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cerounocenter.entidades.CntCuedoc;
import com.cerounocenter.entidades.CntCuedocPK;
import com.cerounocenter.repository.CntCuedocServiceRepository;
import com.cerounocenter.service.CntCuedocService;

@Service("cntCuedocService")
public class CntCuedocServiceImpl implements CntCuedocService {

	@Autowired
	@Qualifier("cntCuedocServiceRepository")
	private CntCuedocServiceRepository cntCuedocRepo;

	@Override
	public List<CntCuedoc> findAllByAnoMesTipDoc(String anoDoc, String mesDoc, String tipDoc) {
		CntCuedocPK pk = new CntCuedocPK();
		pk.setAnoDoc("2018");
		pk.setPerDoc("12");
		pk.setTipDoc("040");
		return cntCuedocRepo.findByCntCuedocPK(pk);
	}

	@Override
	public List<CntCuedoc> findAllFilter(String anoDoc, String mesDoc, String tipDoc) {
		return cntCuedocRepo.findByFilter(anoDoc, mesDoc, tipDoc);
	}

	@Override
	public CntCuedoc registrar(CntCuedoc cuedoc) {
		try {
			return cntCuedocRepo.save(cuedoc);
		} catch (Exception e) {
			System.out.println("Error >> "+e.getMessage());
			return null;
		}
	}

}
