package com.cerounocenter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cerounocenter.entidades.CntCuedoc;
import com.cerounocenter.service.CntCuedocService;

@RestController
@RequestMapping("/main")
public class ControllerMain {

	@Autowired
	@Qualifier("cntCuedocService")
	private CntCuedocService cntCuedocService;
	
	
	@GetMapping("/test")
	public String test() {
		List<CntCuedoc> lista = cntCuedocService.findAllFilter("2018", "12", "040");
		if(lista != null) {
			StringBuilder sb = new StringBuilder("<html>");
			sb.append("<body>").append("<ul>");
			for(CntCuedoc cue:lista) {
				sb.append("<li>").append(cue.getCntCuedocPK().getAnoDoc()).append("</>");
				sb.append("<li>").append(cue.getCntCuedocPK().getNumDoc()).append("</>");
				sb.append("<li>").append(cue.getCntCuedocPK().getTipDoc()).append("</>");
			}
			sb.append("</ul>").append("</body>").append("</html>");
			return sb.toString();
		}else {
			return "No hay datos de consulta";
		}
		
	}
}
