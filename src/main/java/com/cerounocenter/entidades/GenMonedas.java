/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_monedas", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenMonedas.findAll", query = "SELECT g FROM GenMonedas g")
    , @NamedQuery(name = "GenMonedas.findByCodMon", query = "SELECT g FROM GenMonedas g WHERE g.codMon = :codMon")
    , @NamedQuery(name = "GenMonedas.findByDesMon", query = "SELECT g FROM GenMonedas g WHERE g.desMon = :desMon")
    , @NamedQuery(name = "GenMonedas.findByDesPlural", query = "SELECT g FROM GenMonedas g WHERE g.desPlural = :desPlural")
    , @NamedQuery(name = "GenMonedas.findByDesSingular", query = "SELECT g FROM GenMonedas g WHERE g.desSingular = :desSingular")
    , @NamedQuery(name = "GenMonedas.findByCodMext", query = "SELECT g FROM GenMonedas g WHERE g.codMext = :codMext")
    , @NamedQuery(name = "GenMonedas.findByCodDian", query = "SELECT g FROM GenMonedas g WHERE g.codDian = :codDian")})
public class GenMonedas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "cod_mon", nullable = false, length = 2)
    private String codMon;
    @Size(max = 20)
    @Column(name = "des_mon", length = 20)
    private String desMon;
    @Size(max = 20)
    @Column(name = "des_plural", length = 20)
    private String desPlural;
    @Size(max = 20)
    @Column(name = "des_singular", length = 20)
    private String desSingular;
    @Size(max = 3)
    @Column(name = "cod_mext", length = 3)
    private String codMext;
    @Size(max = 3)
    @Column(name = "cod_dian", length = 3)
    private String codDian;
    @OneToMany(mappedBy = "tipMon")
    private List<CntPuc> cntPucList;
    @OneToMany(mappedBy = "indMr")
    private List<CntCuedoc> cntCuedocList;
    @OneToMany(mappedBy = "indMp")
    private List<CntCabdoc> cntCabdocList;

    public GenMonedas() {
    }

    public GenMonedas(String codMon) {
        this.codMon = codMon;
    }

    public String getCodMon() {
        return codMon;
    }

    public void setCodMon(String codMon) {
        this.codMon = codMon;
    }

    public String getDesMon() {
        return desMon;
    }

    public void setDesMon(String desMon) {
        this.desMon = desMon;
    }

    public String getDesPlural() {
        return desPlural;
    }

    public void setDesPlural(String desPlural) {
        this.desPlural = desPlural;
    }

    public String getDesSingular() {
        return desSingular;
    }

    public void setDesSingular(String desSingular) {
        this.desSingular = desSingular;
    }

    public String getCodMext() {
        return codMext;
    }

    public void setCodMext(String codMext) {
        this.codMext = codMext;
    }

    public String getCodDian() {
        return codDian;
    }

    public void setCodDian(String codDian) {
        this.codDian = codDian;
    }

    public List<CntPuc> getCntPucList() {
        return cntPucList;
    }

    public void setCntPucList(List<CntPuc> cntPucList) {
        this.cntPucList = cntPucList;
    }

    public List<CntCuedoc> getCntCuedocList() {
        return cntCuedocList;
    }

    public void setCntCuedocList(List<CntCuedoc> cntCuedocList) {
        this.cntCuedocList = cntCuedocList;
    }

    public List<CntCabdoc> getCntCabdocList() {
        return cntCabdocList;
    }

    public void setCntCabdocList(List<CntCabdoc> cntCabdocList) {
        this.cntCabdocList = cntCabdocList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codMon != null ? codMon.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenMonedas)) {
            return false;
        }
        GenMonedas other = (GenMonedas) object;
        if ((this.codMon == null && other.codMon != null) || (this.codMon != null && !this.codMon.equals(other.codMon))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenMonedas[ codMon=" + codMon + " ]";
    }
    
}
