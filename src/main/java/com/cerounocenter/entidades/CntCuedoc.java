/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "cnt_cuedoc", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "CntCuedoc.findAll", query = "SELECT c FROM CntCuedoc c")
    , @NamedQuery(name = "CntCuedoc.findByAnoDoc", query = "SELECT c FROM CntCuedoc c WHERE c.cntCuedocPK.anoDoc = :anoDoc")
    , @NamedQuery(name = "CntCuedoc.findByPerDoc", query = "SELECT c FROM CntCuedoc c WHERE c.cntCuedocPK.perDoc = :perDoc")
    , @NamedQuery(name = "CntCuedoc.findByTipDoc", query = "SELECT c FROM CntCuedoc c WHERE c.cntCuedocPK.tipDoc = :tipDoc")
    , @NamedQuery(name = "CntCuedoc.findByNumDoc", query = "SELECT c FROM CntCuedoc c WHERE c.cntCuedocPK.numDoc = :numDoc")
    , @NamedQuery(name = "CntCuedoc.findByRegDoc", query = "SELECT c FROM CntCuedoc c WHERE c.cntCuedocPK.regDoc = :regDoc")
    , @NamedQuery(name = "CntCuedoc.findByTrans", query = "SELECT c FROM CntCuedoc c WHERE c.trans = :trans")
    , @NamedQuery(name = "CntCuedoc.findByNumChe", query = "SELECT c FROM CntCuedoc c WHERE c.numChe = :numChe")
    , @NamedQuery(name = "CntCuedoc.findByBasMov", query = "SELECT c FROM CntCuedoc c WHERE c.basMov = :basMov")
    , @NamedQuery(name = "CntCuedoc.findByDesMov", query = "SELECT c FROM CntCuedoc c WHERE c.desMov = :desMov")
    , @NamedQuery(name = "CntCuedoc.findByDebMov", query = "SELECT c FROM CntCuedoc c WHERE c.debMov = :debMov")
    , @NamedQuery(name = "CntCuedoc.findByCreMov", query = "SELECT c FROM CntCuedoc c WHERE c.creMov = :creMov")
    , @NamedQuery(name = "CntCuedoc.findByTipMov", query = "SELECT c FROM CntCuedoc c WHERE c.tipMov = :tipMov")
    , @NamedQuery(name = "CntCuedoc.findByFchMov", query = "SELECT c FROM CntCuedoc c WHERE c.fchMov = :fchMov")
    , @NamedQuery(name = "CntCuedoc.findByCodDif", query = "SELECT c FROM CntCuedoc c WHERE c.codDif = :codDif")
    , @NamedQuery(name = "CntCuedoc.findByDiferencia", query = "SELECT c FROM CntCuedoc c WHERE c.diferencia = :diferencia")
    , @NamedQuery(name = "CntCuedoc.findByDexMov", query = "SELECT c FROM CntCuedoc c WHERE c.dexMov = :dexMov")
    , @NamedQuery(name = "CntCuedoc.findByCexMov", query = "SELECT c FROM CntCuedoc c WHERE c.cexMov = :cexMov")
    , @NamedQuery(name = "CntCuedoc.findByMarca", query = "SELECT c FROM CntCuedoc c WHERE c.marca = :marca")
    , @NamedQuery(name = "CntCuedoc.findByFecTas", query = "SELECT c FROM CntCuedoc c WHERE c.fecTas = :fecTas")
    , @NamedQuery(name = "CntCuedoc.findByTasa", query = "SELECT c FROM CntCuedoc c WHERE c.tasa = :tasa")
    , @NamedQuery(name = "CntCuedoc.findBySubTip", query = "SELECT c FROM CntCuedoc c WHERE c.subTip = :subTip")
    , @NamedQuery(name = "CntCuedoc.findByFecCte", query = "SELECT c FROM CntCuedoc c WHERE c.fecCte = :fecCte")
    , @NamedQuery(name = "CntCuedoc.findByIDLnum", query = "SELECT c FROM CntCuedoc c WHERE c.iDLnum = :iDLnum")
    , @NamedQuery(name = "CntCuedoc.findByCodPlt", query = "SELECT c FROM CntCuedoc c WHERE c.codPlt = :codPlt")})
public class CntCuedoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CntCuedocPK cntCuedocPK;
    @Size(max = 5)
    @Column(name = "trans", length = 5)
    private String trans;
    @Size(max = 10)
    @Column(name = "num_che", length = 10)
    private String numChe;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "bas_mov", precision = 19, scale = 4)
    private BigDecimal basMov;
    @Size(max = 70)
    @Column(name = "des_mov", length = 70)
    private String desMov;
    @Basic(optional = false)
    @NotNull
    @Column(name = "deb_mov", nullable = false, precision = 19, scale = 4)
    private BigDecimal debMov;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cre_mov", nullable = false, precision = 19, scale = 4)
    private BigDecimal creMov;
    @Column(name = "tip_mov")
    private Character tipMov;
    @Column(name = "fch_mov")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fchMov;
    @Size(max = 10)
    @Column(name = "cod_dif", length = 10)
    private String codDif;
    @Column(name = "diferencia", precision = 19, scale = 4)
    private BigDecimal diferencia;
    @Column(name = "dex_mov", precision = 19, scale = 4)
    private BigDecimal dexMov;
    @Column(name = "cex_mov", precision = 19, scale = 4)
    private BigDecimal cexMov;
    @Column(name = "marca")
    private Character marca;
    @Column(name = "fec_tas")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecTas;
    @Column(name = "tasa", precision = 19, scale = 4)
    private BigDecimal tasa;
    @Size(max = 5)
    @Column(name = "sub_tip", length = 5)
    private String subTip;
    @Column(name = "fec_cte")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecCte;
    @Column(name = "IDL_num")
    private BigInteger iDLnum;
    @Size(max = 15)
    @Column(name = "cod_plt", length = 15)
    private String codPlt;
    @JoinColumns({
        @JoinColumn(name = "ano_doc", referencedColumnName = "ano_doc", nullable = false, insertable = false, updatable = false)
        , @JoinColumn(name = "per_doc", referencedColumnName = "per_doc", nullable = false, insertable = false, updatable = false)
        , @JoinColumn(name = "tip_doc", referencedColumnName = "tip_doc", nullable = false, insertable = false, updatable = false)
        , @JoinColumn(name = "num_doc", referencedColumnName = "num_doc", nullable = false, insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CntCabdoc cntCabdoc;
    @JoinColumn(name = "cod_cta", referencedColumnName = "cod_cta", nullable = false)
    @ManyToOne(optional = false)
    private CntPuc codCta;
    @JoinColumn(name = "tip_doc", referencedColumnName = "cod_tip", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CntTipodoc cntTipodoc;
    @JoinColumn(name = "cod_cco", referencedColumnName = "cod_cco")
    @ManyToOne
    private GenCcosto codCco;
    @JoinColumn(name = "cod_cl1", referencedColumnName = "codigo")
    @ManyToOne
    private GenClasif1 codCl1;
    @JoinColumn(name = "cod_cl2", referencedColumnName = "codigo")
    @ManyToOne
    private GenClasif2 codCl2;
    @JoinColumn(name = "cod_cl3", referencedColumnName = "codigo")
    @ManyToOne
    private GenClasif3 codCl3;
    @JoinColumn(name = "ind_mr", referencedColumnName = "cod_mon")
    @ManyToOne
    private GenMonedas indMr;
    @JoinColumn(name = "cod_suc", referencedColumnName = "cod_suc")
    @ManyToOne
    private GenSucursal codSuc;
    @JoinColumn(name = "cod_ter", referencedColumnName = "ter_nit")
    @ManyToOne
    private GenTerceros codTer;

    public CntCuedoc() {
    }

    public CntCuedoc(CntCuedocPK cntCuedocPK) {
        this.cntCuedocPK = cntCuedocPK;
    }

    public CntCuedoc(CntCuedocPK cntCuedocPK, BigDecimal debMov, BigDecimal creMov) {
        this.cntCuedocPK = cntCuedocPK;
        this.debMov = debMov;
        this.creMov = creMov;
    }

    public CntCuedoc(String anoDoc, String perDoc, String tipDoc, String numDoc, int regDoc) {
        this.cntCuedocPK = new CntCuedocPK(anoDoc, perDoc, tipDoc, numDoc, regDoc);
    }

    public CntCuedocPK getCntCuedocPK() {
        return cntCuedocPK;
    }

    public void setCntCuedocPK(CntCuedocPK cntCuedocPK) {
        this.cntCuedocPK = cntCuedocPK;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }

    public String getNumChe() {
        return numChe;
    }

    public void setNumChe(String numChe) {
        this.numChe = numChe;
    }

    public BigDecimal getBasMov() {
        return basMov;
    }

    public void setBasMov(BigDecimal basMov) {
        this.basMov = basMov;
    }

    public String getDesMov() {
        return desMov;
    }

    public void setDesMov(String desMov) {
        this.desMov = desMov;
    }

    public BigDecimal getDebMov() {
        return debMov;
    }

    public void setDebMov(BigDecimal debMov) {
        this.debMov = debMov;
    }

    public BigDecimal getCreMov() {
        return creMov;
    }

    public void setCreMov(BigDecimal creMov) {
        this.creMov = creMov;
    }

    public Character getTipMov() {
        return tipMov;
    }

    public void setTipMov(Character tipMov) {
        this.tipMov = tipMov;
    }

    public Date getFchMov() {
        return fchMov;
    }

    public void setFchMov(Date fchMov) {
        this.fchMov = fchMov;
    }

    public String getCodDif() {
        return codDif;
    }

    public void setCodDif(String codDif) {
        this.codDif = codDif;
    }

    public BigDecimal getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(BigDecimal diferencia) {
        this.diferencia = diferencia;
    }

    public BigDecimal getDexMov() {
        return dexMov;
    }

    public void setDexMov(BigDecimal dexMov) {
        this.dexMov = dexMov;
    }

    public BigDecimal getCexMov() {
        return cexMov;
    }

    public void setCexMov(BigDecimal cexMov) {
        this.cexMov = cexMov;
    }

    public Character getMarca() {
        return marca;
    }

    public void setMarca(Character marca) {
        this.marca = marca;
    }

    public Date getFecTas() {
        return fecTas;
    }

    public void setFecTas(Date fecTas) {
        this.fecTas = fecTas;
    }

    public BigDecimal getTasa() {
        return tasa;
    }

    public void setTasa(BigDecimal tasa) {
        this.tasa = tasa;
    }

    public String getSubTip() {
        return subTip;
    }

    public void setSubTip(String subTip) {
        this.subTip = subTip;
    }

    public Date getFecCte() {
        return fecCte;
    }

    public void setFecCte(Date fecCte) {
        this.fecCte = fecCte;
    }

    public BigInteger getIDLnum() {
        return iDLnum;
    }

    public void setIDLnum(BigInteger iDLnum) {
        this.iDLnum = iDLnum;
    }

    public String getCodPlt() {
        return codPlt;
    }

    public void setCodPlt(String codPlt) {
        this.codPlt = codPlt;
    }

    public CntCabdoc getCntCabdoc() {
        return cntCabdoc;
    }

    public void setCntCabdoc(CntCabdoc cntCabdoc) {
        this.cntCabdoc = cntCabdoc;
    }

    public CntPuc getCodCta() {
        return codCta;
    }

    public void setCodCta(CntPuc codCta) {
        this.codCta = codCta;
    }

    public CntTipodoc getCntTipodoc() {
        return cntTipodoc;
    }

    public void setCntTipodoc(CntTipodoc cntTipodoc) {
        this.cntTipodoc = cntTipodoc;
    }

    public GenCcosto getCodCco() {
        return codCco;
    }

    public void setCodCco(GenCcosto codCco) {
        this.codCco = codCco;
    }

    public GenClasif1 getCodCl1() {
        return codCl1;
    }

    public void setCodCl1(GenClasif1 codCl1) {
        this.codCl1 = codCl1;
    }

    public GenClasif2 getCodCl2() {
        return codCl2;
    }

    public void setCodCl2(GenClasif2 codCl2) {
        this.codCl2 = codCl2;
    }

    public GenClasif3 getCodCl3() {
        return codCl3;
    }

    public void setCodCl3(GenClasif3 codCl3) {
        this.codCl3 = codCl3;
    }

    public GenMonedas getIndMr() {
        return indMr;
    }

    public void setIndMr(GenMonedas indMr) {
        this.indMr = indMr;
    }

    public GenSucursal getCodSuc() {
        return codSuc;
    }

    public void setCodSuc(GenSucursal codSuc) {
        this.codSuc = codSuc;
    }

    public GenTerceros getCodTer() {
        return codTer;
    }

    public void setCodTer(GenTerceros codTer) {
        this.codTer = codTer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cntCuedocPK != null ? cntCuedocPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CntCuedoc)) {
            return false;
        }
        CntCuedoc other = (CntCuedoc) object;
        if ((this.cntCuedocPK == null && other.cntCuedocPK != null) || (this.cntCuedocPK != null && !this.cntCuedocPK.equals(other.cntCuedocPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.CntCuedoc[ cntCuedocPK=" + cntCuedocPK + " ]";
    }
    
}
