/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_ciudad", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenCiudad.findAll", query = "SELECT g FROM GenCiudad g")
    , @NamedQuery(name = "GenCiudad.findByCodPai", query = "SELECT g FROM GenCiudad g WHERE g.genCiudadPK.codPai = :codPai")
    , @NamedQuery(name = "GenCiudad.findByCodDep", query = "SELECT g FROM GenCiudad g WHERE g.genCiudadPK.codDep = :codDep")
    , @NamedQuery(name = "GenCiudad.findByCodCiu", query = "SELECT g FROM GenCiudad g WHERE g.genCiudadPK.codCiu = :codCiu")
    , @NamedQuery(name = "GenCiudad.findByNomCiu", query = "SELECT g FROM GenCiudad g WHERE g.nomCiu = :nomCiu")
    , @NamedQuery(name = "GenCiudad.findByNumHab", query = "SELECT g FROM GenCiudad g WHERE g.numHab = :numHab")
    , @NamedQuery(name = "GenCiudad.findByIndTel", query = "SELECT g FROM GenCiudad g WHERE g.indTel = :indTel")
    , @NamedQuery(name = "GenCiudad.findByIndIca", query = "SELECT g FROM GenCiudad g WHERE g.indIca = :indIca")
    , @NamedQuery(name = "GenCiudad.findByCodBod", query = "SELECT g FROM GenCiudad g WHERE g.codBod = :codBod")})
public class GenCiudad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenCiudadPK genCiudadPK;
    @Size(max = 30)
    @Column(name = "nom_ciu", length = 30)
    private String nomCiu;
    @Column(name = "num_hab")
    private Integer numHab;
    @Size(max = 3)
    @Column(name = "ind_tel", length = 3)
    private String indTel;
    @Column(name = "ind_ica")
    private Boolean indIca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "cod_bod", nullable = false, length = 3)
    private String codBod;
    @JoinColumns({
        @JoinColumn(name = "cod_pai", referencedColumnName = "cod_pai", nullable = false, insertable = false, updatable = false)
        , @JoinColumn(name = "cod_dep", referencedColumnName = "cod_dep", nullable = false, insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private GenDeptos genDeptos;
    @JoinColumn(name = "cod_pai", referencedColumnName = "cod_pai", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private GenPaises genPaises;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genCiudad")
    private List<GenSucursal> genSucursalList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genCiudad")
    private List<GenTerceros> genTercerosList;

    public GenCiudad() {
    }

    public GenCiudad(GenCiudadPK genCiudadPK) {
        this.genCiudadPK = genCiudadPK;
    }

    public GenCiudad(GenCiudadPK genCiudadPK, String codBod) {
        this.genCiudadPK = genCiudadPK;
        this.codBod = codBod;
    }

    public GenCiudad(String codPai, String codDep, String codCiu) {
        this.genCiudadPK = new GenCiudadPK(codPai, codDep, codCiu);
    }

    public GenCiudadPK getGenCiudadPK() {
        return genCiudadPK;
    }

    public void setGenCiudadPK(GenCiudadPK genCiudadPK) {
        this.genCiudadPK = genCiudadPK;
    }

    public String getNomCiu() {
        return nomCiu;
    }

    public void setNomCiu(String nomCiu) {
        this.nomCiu = nomCiu;
    }

    public Integer getNumHab() {
        return numHab;
    }

    public void setNumHab(Integer numHab) {
        this.numHab = numHab;
    }

    public String getIndTel() {
        return indTel;
    }

    public void setIndTel(String indTel) {
        this.indTel = indTel;
    }

    public Boolean getIndIca() {
        return indIca;
    }

    public void setIndIca(Boolean indIca) {
        this.indIca = indIca;
    }

    public String getCodBod() {
        return codBod;
    }

    public void setCodBod(String codBod) {
        this.codBod = codBod;
    }

    public GenDeptos getGenDeptos() {
        return genDeptos;
    }

    public void setGenDeptos(GenDeptos genDeptos) {
        this.genDeptos = genDeptos;
    }

    public GenPaises getGenPaises() {
        return genPaises;
    }

    public void setGenPaises(GenPaises genPaises) {
        this.genPaises = genPaises;
    }

    public List<GenSucursal> getGenSucursalList() {
        return genSucursalList;
    }

    public void setGenSucursalList(List<GenSucursal> genSucursalList) {
        this.genSucursalList = genSucursalList;
    }

    public List<GenTerceros> getGenTercerosList() {
        return genTercerosList;
    }

    public void setGenTercerosList(List<GenTerceros> genTercerosList) {
        this.genTercerosList = genTercerosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genCiudadPK != null ? genCiudadPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenCiudad)) {
            return false;
        }
        GenCiudad other = (GenCiudad) object;
        if ((this.genCiudadPK == null && other.genCiudadPK != null) || (this.genCiudadPK != null && !this.genCiudadPK.equals(other.genCiudadPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenCiudad[ genCiudadPK=" + genCiudadPK + " ]";
    }
    
}
