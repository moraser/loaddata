/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_sucursal", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenSucursal.findAll", query = "SELECT g FROM GenSucursal g")
    , @NamedQuery(name = "GenSucursal.findByCodSuc", query = "SELECT g FROM GenSucursal g WHERE g.codSuc = :codSuc")
    , @NamedQuery(name = "GenSucursal.findByNomSuc", query = "SELECT g FROM GenSucursal g WHERE g.nomSuc = :nomSuc")
    , @NamedQuery(name = "GenSucursal.findByDirSuc", query = "SELECT g FROM GenSucursal g WHERE g.dirSuc = :dirSuc")
    , @NamedQuery(name = "GenSucursal.findByTelSuc", query = "SELECT g FROM GenSucursal g WHERE g.telSuc = :telSuc")
    , @NamedQuery(name = "GenSucursal.findByEncSuc", query = "SELECT g FROM GenSucursal g WHERE g.encSuc = :encSuc")
    , @NamedQuery(name = "GenSucursal.findByEstSuc", query = "SELECT g FROM GenSucursal g WHERE g.estSuc = :estSuc")
    , @NamedQuery(name = "GenSucursal.findByTarIca", query = "SELECT g FROM GenSucursal g WHERE g.tarIca = :tarIca")
    , @NamedQuery(name = "GenSucursal.findByBodFact", query = "SELECT g FROM GenSucursal g WHERE g.bodFact = :bodFact")})
public class GenSucursal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "cod_suc", nullable = false, length = 3)
    private String codSuc;
    @Size(max = 100)
    @Column(name = "nom_suc", length = 100)
    private String nomSuc;
    @Size(max = 40)
    @Column(name = "dir_suc", length = 40)
    private String dirSuc;
    @Size(max = 10)
    @Column(name = "tel_suc", length = 10)
    private String telSuc;
    @Size(max = 40)
    @Column(name = "enc_suc", length = 40)
    private String encSuc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "est_suc", nullable = false)
    private short estSuc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "tar_ica", nullable = false, precision = 19, scale = 4)
    private BigDecimal tarIca;
    @Size(max = 3)
    @Column(name = "bod_fact", length = 3)
    private String bodFact;
    @OneToMany(mappedBy = "codSuc")
    private List<CntCuedoc> cntCuedocList;
    @JoinColumn(name = "cod_cta", referencedColumnName = "cod_cta")
    @ManyToOne
    private CntPuc codCta;
    @JoinColumns({
        @JoinColumn(name = "cod_pai", referencedColumnName = "cod_pai", nullable = false)
        , @JoinColumn(name = "cod_dep", referencedColumnName = "cod_dep")
        , @JoinColumn(name = "cod_ciu", referencedColumnName = "cod_ciu")})
    @ManyToOne(optional = false)
    private GenCiudad genCiudad;
    @JoinColumn(name = "nit_convenio", referencedColumnName = "ter_nit")
    @ManyToOne
    private GenTerceros nitConvenio;

    public GenSucursal() {
    }

    public GenSucursal(String codSuc) {
        this.codSuc = codSuc;
    }

    public GenSucursal(String codSuc, short estSuc, BigDecimal tarIca) {
        this.codSuc = codSuc;
        this.estSuc = estSuc;
        this.tarIca = tarIca;
    }

    public String getCodSuc() {
        return codSuc;
    }

    public void setCodSuc(String codSuc) {
        this.codSuc = codSuc;
    }

    public String getNomSuc() {
        return nomSuc;
    }

    public void setNomSuc(String nomSuc) {
        this.nomSuc = nomSuc;
    }

    public String getDirSuc() {
        return dirSuc;
    }

    public void setDirSuc(String dirSuc) {
        this.dirSuc = dirSuc;
    }

    public String getTelSuc() {
        return telSuc;
    }

    public void setTelSuc(String telSuc) {
        this.telSuc = telSuc;
    }

    public String getEncSuc() {
        return encSuc;
    }

    public void setEncSuc(String encSuc) {
        this.encSuc = encSuc;
    }

    public short getEstSuc() {
        return estSuc;
    }

    public void setEstSuc(short estSuc) {
        this.estSuc = estSuc;
    }

    public BigDecimal getTarIca() {
        return tarIca;
    }

    public void setTarIca(BigDecimal tarIca) {
        this.tarIca = tarIca;
    }

    public String getBodFact() {
        return bodFact;
    }

    public void setBodFact(String bodFact) {
        this.bodFact = bodFact;
    }

    public List<CntCuedoc> getCntCuedocList() {
        return cntCuedocList;
    }

    public void setCntCuedocList(List<CntCuedoc> cntCuedocList) {
        this.cntCuedocList = cntCuedocList;
    }

    public CntPuc getCodCta() {
        return codCta;
    }

    public void setCodCta(CntPuc codCta) {
        this.codCta = codCta;
    }

    public GenCiudad getGenCiudad() {
        return genCiudad;
    }

    public void setGenCiudad(GenCiudad genCiudad) {
        this.genCiudad = genCiudad;
    }

    public GenTerceros getNitConvenio() {
        return nitConvenio;
    }

    public void setNitConvenio(GenTerceros nitConvenio) {
        this.nitConvenio = nitConvenio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codSuc != null ? codSuc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenSucursal)) {
            return false;
        }
        GenSucursal other = (GenSucursal) object;
        if ((this.codSuc == null && other.codSuc != null) || (this.codSuc != null && !this.codSuc.equals(other.codSuc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenSucursal[ codSuc=" + codSuc + " ]";
    }
    
}
