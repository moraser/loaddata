/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Embeddable
public class GenCiudadPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "cod_pai", nullable = false, length = 3)
    private String codPai;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "cod_dep", nullable = false, length = 2)
    private String codDep;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "cod_ciu", nullable = false, length = 5)
    private String codCiu;

    public GenCiudadPK() {
    }

    public GenCiudadPK(String codPai, String codDep, String codCiu) {
        this.codPai = codPai;
        this.codDep = codDep;
        this.codCiu = codCiu;
    }

    public String getCodPai() {
        return codPai;
    }

    public void setCodPai(String codPai) {
        this.codPai = codPai;
    }

    public String getCodDep() {
        return codDep;
    }

    public void setCodDep(String codDep) {
        this.codDep = codDep;
    }

    public String getCodCiu() {
        return codCiu;
    }

    public void setCodCiu(String codCiu) {
        this.codCiu = codCiu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPai != null ? codPai.hashCode() : 0);
        hash += (codDep != null ? codDep.hashCode() : 0);
        hash += (codCiu != null ? codCiu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenCiudadPK)) {
            return false;
        }
        GenCiudadPK other = (GenCiudadPK) object;
        if ((this.codPai == null && other.codPai != null) || (this.codPai != null && !this.codPai.equals(other.codPai))) {
            return false;
        }
        if ((this.codDep == null && other.codDep != null) || (this.codDep != null && !this.codDep.equals(other.codDep))) {
            return false;
        }
        if ((this.codCiu == null && other.codCiu != null) || (this.codCiu != null && !this.codCiu.equals(other.codCiu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenCiudadPK[ codPai=" + codPai + ", codDep=" + codDep + ", codCiu=" + codCiu + " ]";
    }
    
}
