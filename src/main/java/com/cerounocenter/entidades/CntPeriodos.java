/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "cnt_periodos", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "CntPeriodos.findAll", query = "SELECT c FROM CntPeriodos c")
    , @NamedQuery(name = "CntPeriodos.findByCodAno", query = "SELECT c FROM CntPeriodos c WHERE c.cntPeriodosPK.codAno = :codAno")
    , @NamedQuery(name = "CntPeriodos.findByCodPer", query = "SELECT c FROM CntPeriodos c WHERE c.cntPeriodosPK.codPer = :codPer")
    , @NamedQuery(name = "CntPeriodos.findByNomPer", query = "SELECT c FROM CntPeriodos c WHERE c.nomPer = :nomPer")
    , @NamedQuery(name = "CntPeriodos.findByIndBlq", query = "SELECT c FROM CntPeriodos c WHERE c.indBlq = :indBlq")
    , @NamedQuery(name = "CntPeriodos.findByIndAct", query = "SELECT c FROM CntPeriodos c WHERE c.indAct = :indAct")
    , @NamedQuery(name = "CntPeriodos.findByFecIni", query = "SELECT c FROM CntPeriodos c WHERE c.fecIni = :fecIni")
    , @NamedQuery(name = "CntPeriodos.findByFecFin", query = "SELECT c FROM CntPeriodos c WHERE c.fecFin = :fecFin")})
public class CntPeriodos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CntPeriodosPK cntPeriodosPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "nom_per", nullable = false, length = 15)
    private String nomPer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_blq", nullable = false)
    private boolean indBlq;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_act", nullable = false)
    private boolean indAct;
    @Column(name = "fec_ini")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecIni;
    @Column(name = "fec_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecFin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cntPeriodos")
    private List<CntCabdoc> cntCabdocList;

    public CntPeriodos() {
    }

    public CntPeriodos(CntPeriodosPK cntPeriodosPK) {
        this.cntPeriodosPK = cntPeriodosPK;
    }

    public CntPeriodos(CntPeriodosPK cntPeriodosPK, String nomPer, boolean indBlq, boolean indAct) {
        this.cntPeriodosPK = cntPeriodosPK;
        this.nomPer = nomPer;
        this.indBlq = indBlq;
        this.indAct = indAct;
    }

    public CntPeriodos(String codAno, String codPer) {
        this.cntPeriodosPK = new CntPeriodosPK(codAno, codPer);
    }

    public CntPeriodosPK getCntPeriodosPK() {
        return cntPeriodosPK;
    }

    public void setCntPeriodosPK(CntPeriodosPK cntPeriodosPK) {
        this.cntPeriodosPK = cntPeriodosPK;
    }

    public String getNomPer() {
        return nomPer;
    }

    public void setNomPer(String nomPer) {
        this.nomPer = nomPer;
    }

    public boolean getIndBlq() {
        return indBlq;
    }

    public void setIndBlq(boolean indBlq) {
        this.indBlq = indBlq;
    }

    public boolean getIndAct() {
        return indAct;
    }

    public void setIndAct(boolean indAct) {
        this.indAct = indAct;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public List<CntCabdoc> getCntCabdocList() {
        return cntCabdocList;
    }

    public void setCntCabdocList(List<CntCabdoc> cntCabdocList) {
        this.cntCabdocList = cntCabdocList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cntPeriodosPK != null ? cntPeriodosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CntPeriodos)) {
            return false;
        }
        CntPeriodos other = (CntPeriodos) object;
        if ((this.cntPeriodosPK == null && other.cntPeriodosPK != null) || (this.cntPeriodosPK != null && !this.cntPeriodosPK.equals(other.cntPeriodosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.CntPeriodos[ cntPeriodosPK=" + cntPeriodosPK + " ]";
    }
    
}
