/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_deptos", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenDeptos.findAll", query = "SELECT g FROM GenDeptos g")
    , @NamedQuery(name = "GenDeptos.findByCodPai", query = "SELECT g FROM GenDeptos g WHERE g.genDeptosPK.codPai = :codPai")
    , @NamedQuery(name = "GenDeptos.findByCodDep", query = "SELECT g FROM GenDeptos g WHERE g.genDeptosPK.codDep = :codDep")
    , @NamedQuery(name = "GenDeptos.findByNomDep", query = "SELECT g FROM GenDeptos g WHERE g.nomDep = :nomDep")
    , @NamedQuery(name = "GenDeptos.findByIndPol", query = "SELECT g FROM GenDeptos g WHERE g.indPol = :indPol")
    , @NamedQuery(name = "GenDeptos.findBySiglaDep", query = "SELECT g FROM GenDeptos g WHERE g.siglaDep = :siglaDep")})
public class GenDeptos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenDeptosPK genDeptosPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nom_dep", nullable = false, length = 30)
    private String nomDep;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_pol", nullable = false)
    private boolean indPol;
    @Size(max = 5)
    @Column(name = "sigla_dep", length = 5)
    private String siglaDep;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genDeptos")
    private List<GenCiudad> genCiudadList;
    @JoinColumn(name = "cod_pai", referencedColumnName = "cod_pai", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private GenPaises genPaises;

    public GenDeptos() {
    }

    public GenDeptos(GenDeptosPK genDeptosPK) {
        this.genDeptosPK = genDeptosPK;
    }

    public GenDeptos(GenDeptosPK genDeptosPK, String nomDep, boolean indPol) {
        this.genDeptosPK = genDeptosPK;
        this.nomDep = nomDep;
        this.indPol = indPol;
    }

    public GenDeptos(String codPai, String codDep) {
        this.genDeptosPK = new GenDeptosPK(codPai, codDep);
    }

    public GenDeptosPK getGenDeptosPK() {
        return genDeptosPK;
    }

    public void setGenDeptosPK(GenDeptosPK genDeptosPK) {
        this.genDeptosPK = genDeptosPK;
    }

    public String getNomDep() {
        return nomDep;
    }

    public void setNomDep(String nomDep) {
        this.nomDep = nomDep;
    }

    public boolean getIndPol() {
        return indPol;
    }

    public void setIndPol(boolean indPol) {
        this.indPol = indPol;
    }

    public String getSiglaDep() {
        return siglaDep;
    }

    public void setSiglaDep(String siglaDep) {
        this.siglaDep = siglaDep;
    }

    public List<GenCiudad> getGenCiudadList() {
        return genCiudadList;
    }

    public void setGenCiudadList(List<GenCiudad> genCiudadList) {
        this.genCiudadList = genCiudadList;
    }

    public GenPaises getGenPaises() {
        return genPaises;
    }

    public void setGenPaises(GenPaises genPaises) {
        this.genPaises = genPaises;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genDeptosPK != null ? genDeptosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenDeptos)) {
            return false;
        }
        GenDeptos other = (GenDeptos) object;
        if ((this.genDeptosPK == null && other.genDeptosPK != null) || (this.genDeptosPK != null && !this.genDeptosPK.equals(other.genDeptosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenDeptos[ genDeptosPK=" + genDeptosPK + " ]";
    }
    
}
