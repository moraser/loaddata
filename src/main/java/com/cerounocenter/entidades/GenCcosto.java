/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_ccosto", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenCcosto.findAll", query = "SELECT g FROM GenCcosto g")
    , @NamedQuery(name = "GenCcosto.findByCodCco", query = "SELECT g FROM GenCcosto g WHERE g.codCco = :codCco")
    , @NamedQuery(name = "GenCcosto.findByNomCco", query = "SELECT g FROM GenCcosto g WHERE g.nomCco = :nomCco")
    , @NamedQuery(name = "GenCcosto.findByEstCco", query = "SELECT g FROM GenCcosto g WHERE g.estCco = :estCco")})
public class GenCcosto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "cod_cco", nullable = false, length = 10)
    private String codCco;
    @Size(max = 100)
    @Column(name = "nom_cco", length = 100)
    private String nomCco;
    @Basic(optional = false)
    @NotNull
    @Column(name = "est_cco", nullable = false)
    private short estCco;
    @OneToMany(mappedBy = "codCco")
    private List<CntCuedoc> cntCuedocList;

    public GenCcosto() {
    }

    public GenCcosto(String codCco) {
        this.codCco = codCco;
    }

    public GenCcosto(String codCco, short estCco) {
        this.codCco = codCco;
        this.estCco = estCco;
    }

    public String getCodCco() {
        return codCco;
    }

    public void setCodCco(String codCco) {
        this.codCco = codCco;
    }

    public String getNomCco() {
        return nomCco;
    }

    public void setNomCco(String nomCco) {
        this.nomCco = nomCco;
    }

    public short getEstCco() {
        return estCco;
    }

    public void setEstCco(short estCco) {
        this.estCco = estCco;
    }

    public List<CntCuedoc> getCntCuedocList() {
        return cntCuedocList;
    }

    public void setCntCuedocList(List<CntCuedoc> cntCuedocList) {
        this.cntCuedocList = cntCuedocList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCco != null ? codCco.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenCcosto)) {
            return false;
        }
        GenCcosto other = (GenCcosto) object;
        if ((this.codCco == null && other.codCco != null) || (this.codCco != null && !this.codCco.equals(other.codCco))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenCcosto[ codCco=" + codCco + " ]";
    }
    
}
