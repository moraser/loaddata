/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Embeddable
public class CntCuedocPK implements Serializable {

    private static final long serialVersionUID = 1L;
	@Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "ano_doc", nullable = false, length = 4)
    private String anoDoc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "per_doc", nullable = false, length = 2)
    private String perDoc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "tip_doc", nullable = false, length = 3)
    private String tipDoc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "num_doc", nullable = false, length = 14)
    private String numDoc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reg_doc", nullable = false)
    private int regDoc;

    public CntCuedocPK() {
    }

    public CntCuedocPK(String anoDoc, String perDoc, String tipDoc, String numDoc, int regDoc) {
        this.anoDoc = anoDoc;
        this.perDoc = perDoc;
        this.tipDoc = tipDoc;
        this.numDoc = numDoc;
        this.regDoc = regDoc;
    }

    public String getAnoDoc() {
        return anoDoc;
    }

    public void setAnoDoc(String anoDoc) {
        this.anoDoc = anoDoc;
    }

    public String getPerDoc() {
        return perDoc;
    }

    public void setPerDoc(String perDoc) {
        this.perDoc = perDoc;
    }

    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public int getRegDoc() {
        return regDoc;
    }

    public void setRegDoc(int regDoc) {
        this.regDoc = regDoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anoDoc != null ? anoDoc.hashCode() : 0);
        hash += (perDoc != null ? perDoc.hashCode() : 0);
        hash += (tipDoc != null ? tipDoc.hashCode() : 0);
        hash += (numDoc != null ? numDoc.hashCode() : 0);
        hash += (int) regDoc;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CntCuedocPK)) {
            return false;
        }
        CntCuedocPK other = (CntCuedocPK) object;
        if ((this.anoDoc == null && other.anoDoc != null) || (this.anoDoc != null && !this.anoDoc.equals(other.anoDoc))) {
            return false;
        }
        if ((this.perDoc == null && other.perDoc != null) || (this.perDoc != null && !this.perDoc.equals(other.perDoc))) {
            return false;
        }
        if ((this.tipDoc == null && other.tipDoc != null) || (this.tipDoc != null && !this.tipDoc.equals(other.tipDoc))) {
            return false;
        }
        if ((this.numDoc == null && other.numDoc != null) || (this.numDoc != null && !this.numDoc.equals(other.numDoc))) {
            return false;
        }
        if (this.regDoc != other.regDoc) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.CntCuedocPK[ anoDoc=" + anoDoc + ", perDoc=" + perDoc + ", tipDoc=" + tipDoc + ", numDoc=" + numDoc + ", regDoc=" + regDoc + " ]";
    }
    
}
