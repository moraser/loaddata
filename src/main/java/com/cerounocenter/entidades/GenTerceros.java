/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_terceros", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenTerceros.findAll", query = "SELECT g FROM GenTerceros g")
    , @NamedQuery(name = "GenTerceros.findByTerNit", query = "SELECT g FROM GenTerceros g WHERE g.terNit = :terNit")
    , @NamedQuery(name = "GenTerceros.findByTerNombre", query = "SELECT g FROM GenTerceros g WHERE g.terNombre = :terNombre")
    , @NamedQuery(name = "GenTerceros.findByTerDireccion", query = "SELECT g FROM GenTerceros g WHERE g.terDireccion = :terDireccion")
    , @NamedQuery(name = "GenTerceros.findByTerTelefono", query = "SELECT g FROM GenTerceros g WHERE g.terTelefono = :terTelefono")
    , @NamedQuery(name = "GenTerceros.findByTerFechaing", query = "SELECT g FROM GenTerceros g WHERE g.terFechaing = :terFechaing")
    , @NamedQuery(name = "GenTerceros.findByTerRegimen", query = "SELECT g FROM GenTerceros g WHERE g.terRegimen = :terRegimen")
    , @NamedQuery(name = "GenTerceros.findByTerResolucion", query = "SELECT g FROM GenTerceros g WHERE g.terResolucion = :terResolucion")
    , @NamedQuery(name = "GenTerceros.findByTerTipo", query = "SELECT g FROM GenTerceros g WHERE g.terTipo = :terTipo")
    , @NamedQuery(name = "GenTerceros.findByTerDigito", query = "SELECT g FROM GenTerceros g WHERE g.terDigito = :terDigito")
    , @NamedQuery(name = "GenTerceros.findByTerAuto", query = "SELECT g FROM GenTerceros g WHERE g.terAuto = :terAuto")
    , @NamedQuery(name = "GenTerceros.findByIndEmp", query = "SELECT g FROM GenTerceros g WHERE g.indEmp = :indEmp")
    , @NamedQuery(name = "GenTerceros.findByIndCxc", query = "SELECT g FROM GenTerceros g WHERE g.indCxc = :indCxc")
    , @NamedQuery(name = "GenTerceros.findByIndCxp", query = "SELECT g FROM GenTerceros g WHERE g.indCxp = :indCxp")
    , @NamedQuery(name = "GenTerceros.findByIndOtros", query = "SELECT g FROM GenTerceros g WHERE g.indOtros = :indOtros")
    , @NamedQuery(name = "GenTerceros.findByCodSuc", query = "SELECT g FROM GenTerceros g WHERE g.codSuc = :codSuc")
    , @NamedQuery(name = "GenTerceros.findByCodCco", query = "SELECT g FROM GenTerceros g WHERE g.codCco = :codCco")
    , @NamedQuery(name = "GenTerceros.findByCodCl1", query = "SELECT g FROM GenTerceros g WHERE g.codCl1 = :codCl1")
    , @NamedQuery(name = "GenTerceros.findByCodCl2", query = "SELECT g FROM GenTerceros g WHERE g.codCl2 = :codCl2")
    , @NamedQuery(name = "GenTerceros.findByCodCl3", query = "SELECT g FROM GenTerceros g WHERE g.codCl3 = :codCl3")
    , @NamedQuery(name = "GenTerceros.findByEstTer", query = "SELECT g FROM GenTerceros g WHERE g.estTer = :estTer")
    , @NamedQuery(name = "GenTerceros.findByEMail", query = "SELECT g FROM GenTerceros g WHERE g.eMail = :eMail")
    , @NamedQuery(name = "GenTerceros.findByPagWeb", query = "SELECT g FROM GenTerceros g WHERE g.pagWeb = :pagWeb")
    , @NamedQuery(name = "GenTerceros.findByFaxTer", query = "SELECT g FROM GenTerceros g WHERE g.faxTer = :faxTer")
    , @NamedQuery(name = "GenTerceros.findByApaTer", query = "SELECT g FROM GenTerceros g WHERE g.apaTer = :apaTer")
    , @NamedQuery(name = "GenTerceros.findByTelTe2", query = "SELECT g FROM GenTerceros g WHERE g.telTe2 = :telTe2")
    , @NamedQuery(name = "GenTerceros.findByTerciudadania", query = "SELECT g FROM GenTerceros g WHERE g.terciudadania = :terciudadania")
    , @NamedQuery(name = "GenTerceros.findByAp1Ter", query = "SELECT g FROM GenTerceros g WHERE g.ap1Ter = :ap1Ter")
    , @NamedQuery(name = "GenTerceros.findByAp2Ter", query = "SELECT g FROM GenTerceros g WHERE g.ap2Ter = :ap2Ter")
    , @NamedQuery(name = "GenTerceros.findByNom1Ter", query = "SELECT g FROM GenTerceros g WHERE g.nom1Ter = :nom1Ter")
    , @NamedQuery(name = "GenTerceros.findByNom2Ter", query = "SELECT g FROM GenTerceros g WHERE g.nom2Ter = :nom2Ter")
    , @NamedQuery(name = "GenTerceros.findByTipPer", query = "SELECT g FROM GenTerceros g WHERE g.tipPer = :tipPer")
    , @NamedQuery(name = "GenTerceros.findByCodAct", query = "SELECT g FROM GenTerceros g WHERE g.codAct = :codAct")})
public class GenTerceros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "ter_nit", nullable = false, length = 15)
    private String terNit;
    @Size(max = 200)
    @Column(name = "ter_nombre", length = 200)
    private String terNombre;
    @Size(max = 40)
    @Column(name = "ter_direccion", length = 40)
    private String terDireccion;
    @Size(max = 50)
    @Column(name = "ter_telefono", length = 50)
    private String terTelefono;
    @Column(name = "ter_fechaing")
    @Temporal(TemporalType.TIMESTAMP)
    private Date terFechaing;
    @Column(name = "ter_regimen")
    private Short terRegimen;
    @Size(max = 10)
    @Column(name = "ter_resolucion", length = 10)
    private String terResolucion;
    @Column(name = "ter_tipo")
    private Short terTipo;
    @Column(name = "ter_digito")
    private Character terDigito;
    @Column(name = "ter_auto")
    private Short terAuto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_emp", nullable = false)
    private boolean indEmp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_cxc", nullable = false)
    private boolean indCxc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_cxp", nullable = false)
    private boolean indCxp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_otros", nullable = false)
    private boolean indOtros;
    @Size(max = 3)
    @Column(name = "cod_suc", length = 3)
    private String codSuc;
    @Size(max = 6)
    @Column(name = "cod_cco", length = 6)
    private String codCco;
    @Size(max = 10)
    @Column(name = "cod_cl1", length = 10)
    private String codCl1;
    @Size(max = 10)
    @Column(name = "cod_cl2", length = 10)
    private String codCl2;
    @Size(max = 10)
    @Column(name = "cod_cl3", length = 10)
    private String codCl3;
    @Column(name = "est_ter")
    private Short estTer;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Correo electrónico no válido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "e_mail", length = 100)
    private String eMail;
    @Size(max = 40)
    @Column(name = "pag_web", length = 40)
    private String pagWeb;
    @Size(max = 15)
    @Column(name = "fax_ter", length = 15)
    private String faxTer;
    @Size(max = 10)
    @Column(name = "apa_ter", length = 10)
    private String apaTer;
    @Size(max = 15)
    @Column(name = "tel_te2", length = 15)
    private String telTe2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ter_ciudadania", nullable = false)
    private short terciudadania;
    @Size(max = 50)
    @Column(name = "ap1_ter", length = 50)
    private String ap1Ter;
    @Size(max = 50)
    @Column(name = "ap2_ter", length = 50)
    private String ap2Ter;
    @Size(max = 50)
    @Column(name = "nom1_ter", length = 50)
    private String nom1Ter;
    @Size(max = 50)
    @Column(name = "nom2_ter", length = 50)
    private String nom2Ter;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tip_per", nullable = false)
    private short tipPer;
    @Size(max = 10)
    @Column(name = "cod_act", length = 10)
    private String codAct;
    @OneToMany(mappedBy = "codTer")
    private List<CntCuedoc> cntCuedocList;
    @OneToMany(mappedBy = "nitConvenio")
    private List<GenSucursal> genSucursalList;
    @JoinColumns({
        @JoinColumn(name = "cod_pai", referencedColumnName = "cod_pai", nullable = false)
        , @JoinColumn(name = "cod_dep", referencedColumnName = "cod_dep", nullable = false)
        , @JoinColumn(name = "ter_ciu", referencedColumnName = "cod_ciu", nullable = false)})
    @ManyToOne(optional = false)
    private GenCiudad genCiudad;
    @JoinColumn(name = "tip_ide", referencedColumnName = "cod_tip")
    @ManyToOne
    private GenTipide tipIde;

    public GenTerceros() {
    }

    public GenTerceros(String terNit) {
        this.terNit = terNit;
    }

    public GenTerceros(String terNit, boolean indEmp, boolean indCxc, boolean indCxp, boolean indOtros, short terciudadania, short tipPer) {
        this.terNit = terNit;
        this.indEmp = indEmp;
        this.indCxc = indCxc;
        this.indCxp = indCxp;
        this.indOtros = indOtros;
        this.terciudadania = terciudadania;
        this.tipPer = tipPer;
    }

    public String getTerNit() {
        return terNit;
    }

    public void setTerNit(String terNit) {
        this.terNit = terNit;
    }

    public String getTerNombre() {
        return terNombre;
    }

    public void setTerNombre(String terNombre) {
        this.terNombre = terNombre;
    }

    public String getTerDireccion() {
        return terDireccion;
    }

    public void setTerDireccion(String terDireccion) {
        this.terDireccion = terDireccion;
    }

    public String getTerTelefono() {
        return terTelefono;
    }

    public void setTerTelefono(String terTelefono) {
        this.terTelefono = terTelefono;
    }

    public Date getTerFechaing() {
        return terFechaing;
    }

    public void setTerFechaing(Date terFechaing) {
        this.terFechaing = terFechaing;
    }

    public Short getTerRegimen() {
        return terRegimen;
    }

    public void setTerRegimen(Short terRegimen) {
        this.terRegimen = terRegimen;
    }

    public String getTerResolucion() {
        return terResolucion;
    }

    public void setTerResolucion(String terResolucion) {
        this.terResolucion = terResolucion;
    }

    public Short getTerTipo() {
        return terTipo;
    }

    public void setTerTipo(Short terTipo) {
        this.terTipo = terTipo;
    }

    public Character getTerDigito() {
        return terDigito;
    }

    public void setTerDigito(Character terDigito) {
        this.terDigito = terDigito;
    }

    public Short getTerAuto() {
        return terAuto;
    }

    public void setTerAuto(Short terAuto) {
        this.terAuto = terAuto;
    }

    public boolean getIndEmp() {
        return indEmp;
    }

    public void setIndEmp(boolean indEmp) {
        this.indEmp = indEmp;
    }

    public boolean getIndCxc() {
        return indCxc;
    }

    public void setIndCxc(boolean indCxc) {
        this.indCxc = indCxc;
    }

    public boolean getIndCxp() {
        return indCxp;
    }

    public void setIndCxp(boolean indCxp) {
        this.indCxp = indCxp;
    }

    public boolean getIndOtros() {
        return indOtros;
    }

    public void setIndOtros(boolean indOtros) {
        this.indOtros = indOtros;
    }

    public String getCodSuc() {
        return codSuc;
    }

    public void setCodSuc(String codSuc) {
        this.codSuc = codSuc;
    }

    public String getCodCco() {
        return codCco;
    }

    public void setCodCco(String codCco) {
        this.codCco = codCco;
    }

    public String getCodCl1() {
        return codCl1;
    }

    public void setCodCl1(String codCl1) {
        this.codCl1 = codCl1;
    }

    public String getCodCl2() {
        return codCl2;
    }

    public void setCodCl2(String codCl2) {
        this.codCl2 = codCl2;
    }

    public String getCodCl3() {
        return codCl3;
    }

    public void setCodCl3(String codCl3) {
        this.codCl3 = codCl3;
    }

    public Short getEstTer() {
        return estTer;
    }

    public void setEstTer(Short estTer) {
        this.estTer = estTer;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPagWeb() {
        return pagWeb;
    }

    public void setPagWeb(String pagWeb) {
        this.pagWeb = pagWeb;
    }

    public String getFaxTer() {
        return faxTer;
    }

    public void setFaxTer(String faxTer) {
        this.faxTer = faxTer;
    }

    public String getApaTer() {
        return apaTer;
    }

    public void setApaTer(String apaTer) {
        this.apaTer = apaTer;
    }

    public String getTelTe2() {
        return telTe2;
    }

    public void setTelTe2(String telTe2) {
        this.telTe2 = telTe2;
    }

    public short getTerciudadania() {
        return terciudadania;
    }

    public void setTerciudadania(short terciudadania) {
        this.terciudadania = terciudadania;
    }

    public String getAp1Ter() {
        return ap1Ter;
    }

    public void setAp1Ter(String ap1Ter) {
        this.ap1Ter = ap1Ter;
    }

    public String getAp2Ter() {
        return ap2Ter;
    }

    public void setAp2Ter(String ap2Ter) {
        this.ap2Ter = ap2Ter;
    }

    public String getNom1Ter() {
        return nom1Ter;
    }

    public void setNom1Ter(String nom1Ter) {
        this.nom1Ter = nom1Ter;
    }

    public String getNom2Ter() {
        return nom2Ter;
    }

    public void setNom2Ter(String nom2Ter) {
        this.nom2Ter = nom2Ter;
    }

    public short getTipPer() {
        return tipPer;
    }

    public void setTipPer(short tipPer) {
        this.tipPer = tipPer;
    }

    public String getCodAct() {
        return codAct;
    }

    public void setCodAct(String codAct) {
        this.codAct = codAct;
    }

    public List<CntCuedoc> getCntCuedocList() {
        return cntCuedocList;
    }

    public void setCntCuedocList(List<CntCuedoc> cntCuedocList) {
        this.cntCuedocList = cntCuedocList;
    }

    public List<GenSucursal> getGenSucursalList() {
        return genSucursalList;
    }

    public void setGenSucursalList(List<GenSucursal> genSucursalList) {
        this.genSucursalList = genSucursalList;
    }

    public GenCiudad getGenCiudad() {
        return genCiudad;
    }

    public void setGenCiudad(GenCiudad genCiudad) {
        this.genCiudad = genCiudad;
    }

    public GenTipide getTipIde() {
        return tipIde;
    }

    public void setTipIde(GenTipide tipIde) {
        this.tipIde = tipIde;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (terNit != null ? terNit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenTerceros)) {
            return false;
        }
        GenTerceros other = (GenTerceros) object;
        if ((this.terNit == null && other.terNit != null) || (this.terNit != null && !this.terNit.equals(other.terNit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenTerceros[ terNit=" + terNit + " ]";
    }
    
}
