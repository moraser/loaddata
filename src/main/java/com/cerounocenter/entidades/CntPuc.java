/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "cnt_puc", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "CntPuc.findAll", query = "SELECT c FROM CntPuc c")
    , @NamedQuery(name = "CntPuc.findByCodCta", query = "SELECT c FROM CntPuc c WHERE c.codCta = :codCta")
    , @NamedQuery(name = "CntPuc.findByNomCta", query = "SELECT c FROM CntPuc c WHERE c.nomCta = :nomCta")
    , @NamedQuery(name = "CntPuc.findByTipCta", query = "SELECT c FROM CntPuc c WHERE c.tipCta = :tipCta")
    , @NamedQuery(name = "CntPuc.findByNatCta", query = "SELECT c FROM CntPuc c WHERE c.natCta = :natCta")
    , @NamedQuery(name = "CntPuc.findByNivCta", query = "SELECT c FROM CntPuc c WHERE c.nivCta = :nivCta")
    , @NamedQuery(name = "CntPuc.findByIndBas", query = "SELECT c FROM CntPuc c WHERE c.indBas = :indBas")
    , @NamedQuery(name = "CntPuc.findByIndCco", query = "SELECT c FROM CntPuc c WHERE c.indCco = :indCco")
    , @NamedQuery(name = "CntPuc.findByIndTer", query = "SELECT c FROM CntPuc c WHERE c.indTer = :indTer")
    , @NamedQuery(name = "CntPuc.findByIndAju", query = "SELECT c FROM CntPuc c WHERE c.indAju = :indAju")
    , @NamedQuery(name = "CntPuc.findByPorCta", query = "SELECT c FROM CntPuc c WHERE c.porCta = :porCta")
    , @NamedQuery(name = "CntPuc.findByCtaAju", query = "SELECT c FROM CntPuc c WHERE c.ctaAju = :ctaAju")
    , @NamedQuery(name = "CntPuc.findByCtaMon", query = "SELECT c FROM CntPuc c WHERE c.ctaMon = :ctaMon")
    , @NamedQuery(name = "CntPuc.findByIndCon", query = "SELECT c FROM CntPuc c WHERE c.indCon = :indCon")
    , @NamedQuery(name = "CntPuc.findByMarHis", query = "SELECT c FROM CntPuc c WHERE c.marHis = :marHis")
    , @NamedQuery(name = "CntPuc.findByCodCla", query = "SELECT c FROM CntPuc c WHERE c.codCla = :codCla")
    , @NamedQuery(name = "CntPuc.findByCodDian", query = "SELECT c FROM CntPuc c WHERE c.codDian = :codDian")
    , @NamedQuery(name = "CntPuc.findByIndPresup", query = "SELECT c FROM CntPuc c WHERE c.indPresup = :indPresup")
    , @NamedQuery(name = "CntPuc.findByCodSup", query = "SELECT c FROM CntPuc c WHERE c.codSup = :codSup")
    , @NamedQuery(name = "CntPuc.findByCodJun", query = "SELECT c FROM CntPuc c WHERE c.codJun = :codJun")
    , @NamedQuery(name = "CntPuc.findByConcepCta", query = "SELECT c FROM CntPuc c WHERE c.concepCta = :concepCta")
    , @NamedQuery(name = "CntPuc.findByIndInflac", query = "SELECT c FROM CntPuc c WHERE c.indInflac = :indInflac")
    , @NamedQuery(name = "CntPuc.findByCodFlu", query = "SELECT c FROM CntPuc c WHERE c.codFlu = :codFlu")
    , @NamedQuery(name = "CntPuc.findByCodArea", query = "SELECT c FROM CntPuc c WHERE c.codArea = :codArea")
    , @NamedQuery(name = "CntPuc.findByCodRenta", query = "SELECT c FROM CntPuc c WHERE c.codRenta = :codRenta")
    , @NamedQuery(name = "CntPuc.findByIndCl1", query = "SELECT c FROM CntPuc c WHERE c.indCl1 = :indCl1")
    , @NamedQuery(name = "CntPuc.findByIndCl2", query = "SELECT c FROM CntPuc c WHERE c.indCl2 = :indCl2")
    , @NamedQuery(name = "CntPuc.findByIndCl3", query = "SELECT c FROM CntPuc c WHERE c.indCl3 = :indCl3")
    , @NamedQuery(name = "CntPuc.findByDigChe", query = "SELECT c FROM CntPuc c WHERE c.digChe = :digChe")
    , @NamedQuery(name = "CntPuc.findByEstCta", query = "SELECT c FROM CntPuc c WHERE c.estCta = :estCta")
    , @NamedQuery(name = "CntPuc.findByCodAgr", query = "SELECT c FROM CntPuc c WHERE c.codAgr = :codAgr")
    , @NamedQuery(name = "CntPuc.findByCtaMat", query = "SELECT c FROM CntPuc c WHERE c.ctaMat = :ctaMat")
    , @NamedQuery(name = "CntPuc.findByNomMat", query = "SELECT c FROM CntPuc c WHERE c.nomMat = :nomMat")
    , @NamedQuery(name = "CntPuc.findByIndAdc", query = "SELECT c FROM CntPuc c WHERE c.indAdc = :indAdc")
    , @NamedQuery(name = "CntPuc.findByCtaIdc", query = "SELECT c FROM CntPuc c WHERE c.ctaIdc = :ctaIdc")
    , @NamedQuery(name = "CntPuc.findByCtaGdc", query = "SELECT c FROM CntPuc c WHERE c.ctaGdc = :ctaGdc")
    , @NamedQuery(name = "CntPuc.findByIndTas", query = "SELECT c FROM CntPuc c WHERE c.indTas = :indTas")
    , @NamedQuery(name = "CntPuc.findByIndBal", query = "SELECT c FROM CntPuc c WHERE c.indBal = :indBal")
    , @NamedQuery(name = "CntPuc.findByIndDif", query = "SELECT c FROM CntPuc c WHERE c.indDif = :indDif")
    , @NamedQuery(name = "CntPuc.findByIndCor", query = "SELECT c FROM CntPuc c WHERE c.indCor = :indCor")})
public class CntPuc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "cod_cta", nullable = false, length = 16)
    private String codCta;
    @Size(max = 80)
    @Column(name = "nom_cta", length = 80)
    private String nomCta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tip_cta", nullable = false)
    private short tipCta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nat_cta", nullable = false)
    private short natCta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "niv_cta", nullable = false)
    private short nivCta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_bas", nullable = false)
    private short indBas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_cco", nullable = false)
    private short indCco;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_ter", nullable = false)
    private short indTer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_aju", nullable = false)
    private short indAju;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "por_cta", nullable = false, precision = 19, scale = 4)
    private BigDecimal porCta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "cta_aju", nullable = false, length = 16)
    private String ctaAju;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "cta_mon", nullable = false, length = 16)
    private String ctaMon;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_con", nullable = false)
    private short indCon;
    @Column(name = "mar_his")
    private Short marHis;
    @Column(name = "cod_cla")
    private Character codCla;
    @Size(max = 4)
    @Column(name = "cod_dian", length = 4)
    private String codDian;
    @Column(name = "ind_presup")
    private Short indPresup;
    @Size(max = 6)
    @Column(name = "cod_sup", length = 6)
    private String codSup;
    @Size(max = 4)
    @Column(name = "cod_jun", length = 4)
    private String codJun;
    @Size(max = 25)
    @Column(name = "concep_cta", length = 25)
    private String concepCta;
    @Column(name = "ind_inflac")
    private Short indInflac;
    @Size(max = 4)
    @Column(name = "cod_flu", length = 4)
    private String codFlu;
    @Size(max = 4)
    @Column(name = "cod_area", length = 4)
    private String codArea;
    @Size(max = 2)
    @Column(name = "cod_renta", length = 2)
    private String codRenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_cl1", nullable = false)
    private short indCl1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_cl2", nullable = false)
    private short indCl2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_cl3", nullable = false)
    private short indCl3;
    @Column(name = "dig_che")
    private Short digChe;
    @Column(name = "est_cta")
    private Short estCta;
    @Size(max = 10)
    @Column(name = "cod_agr", length = 10)
    private String codAgr;
    @Size(max = 16)
    @Column(name = "cta_mat", length = 16)
    private String ctaMat;
    @Size(max = 30)
    @Column(name = "nom_mat", length = 30)
    private String nomMat;
    @Column(name = "ind_adc")
    private Short indAdc;
    @Size(max = 16)
    @Column(name = "cta_idc", length = 16)
    private String ctaIdc;
    @Size(max = 16)
    @Column(name = "cta_gdc", length = 16)
    private String ctaGdc;
    @Column(name = "ind_tas")
    private Short indTas;
    @Column(name = "ind_bal")
    private Short indBal;
    @Column(name = "ind_dif")
    private Short indDif;
    @Column(name = "ind_cor")
    private Short indCor;
    @JoinColumn(name = "tip_mon", referencedColumnName = "cod_mon")
    @ManyToOne
    private GenMonedas tipMon;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCta")
    private List<CntCuedoc> cntCuedocList;
    @OneToMany(mappedBy = "codCta")
    private List<GenSucursal> genSucursalList;

    public CntPuc() {
    }

    public CntPuc(String codCta) {
        this.codCta = codCta;
    }

    public CntPuc(String codCta, short tipCta, short natCta, short nivCta, short indBas, short indCco, short indTer, short indAju, BigDecimal porCta, String ctaAju, String ctaMon, short indCon, short indCl1, short indCl2, short indCl3) {
        this.codCta = codCta;
        this.tipCta = tipCta;
        this.natCta = natCta;
        this.nivCta = nivCta;
        this.indBas = indBas;
        this.indCco = indCco;
        this.indTer = indTer;
        this.indAju = indAju;
        this.porCta = porCta;
        this.ctaAju = ctaAju;
        this.ctaMon = ctaMon;
        this.indCon = indCon;
        this.indCl1 = indCl1;
        this.indCl2 = indCl2;
        this.indCl3 = indCl3;
    }

    public String getCodCta() {
        return codCta;
    }

    public void setCodCta(String codCta) {
        this.codCta = codCta;
    }

    public String getNomCta() {
        return nomCta;
    }

    public void setNomCta(String nomCta) {
        this.nomCta = nomCta;
    }

    public short getTipCta() {
        return tipCta;
    }

    public void setTipCta(short tipCta) {
        this.tipCta = tipCta;
    }

    public short getNatCta() {
        return natCta;
    }

    public void setNatCta(short natCta) {
        this.natCta = natCta;
    }

    public short getNivCta() {
        return nivCta;
    }

    public void setNivCta(short nivCta) {
        this.nivCta = nivCta;
    }

    public short getIndBas() {
        return indBas;
    }

    public void setIndBas(short indBas) {
        this.indBas = indBas;
    }

    public short getIndCco() {
        return indCco;
    }

    public void setIndCco(short indCco) {
        this.indCco = indCco;
    }

    public short getIndTer() {
        return indTer;
    }

    public void setIndTer(short indTer) {
        this.indTer = indTer;
    }

    public short getIndAju() {
        return indAju;
    }

    public void setIndAju(short indAju) {
        this.indAju = indAju;
    }

    public BigDecimal getPorCta() {
        return porCta;
    }

    public void setPorCta(BigDecimal porCta) {
        this.porCta = porCta;
    }

    public String getCtaAju() {
        return ctaAju;
    }

    public void setCtaAju(String ctaAju) {
        this.ctaAju = ctaAju;
    }

    public String getCtaMon() {
        return ctaMon;
    }

    public void setCtaMon(String ctaMon) {
        this.ctaMon = ctaMon;
    }

    public short getIndCon() {
        return indCon;
    }

    public void setIndCon(short indCon) {
        this.indCon = indCon;
    }

    public Short getMarHis() {
        return marHis;
    }

    public void setMarHis(Short marHis) {
        this.marHis = marHis;
    }

    public Character getCodCla() {
        return codCla;
    }

    public void setCodCla(Character codCla) {
        this.codCla = codCla;
    }

    public String getCodDian() {
        return codDian;
    }

    public void setCodDian(String codDian) {
        this.codDian = codDian;
    }

    public Short getIndPresup() {
        return indPresup;
    }

    public void setIndPresup(Short indPresup) {
        this.indPresup = indPresup;
    }

    public String getCodSup() {
        return codSup;
    }

    public void setCodSup(String codSup) {
        this.codSup = codSup;
    }

    public String getCodJun() {
        return codJun;
    }

    public void setCodJun(String codJun) {
        this.codJun = codJun;
    }

    public String getConcepCta() {
        return concepCta;
    }

    public void setConcepCta(String concepCta) {
        this.concepCta = concepCta;
    }

    public Short getIndInflac() {
        return indInflac;
    }

    public void setIndInflac(Short indInflac) {
        this.indInflac = indInflac;
    }

    public String getCodFlu() {
        return codFlu;
    }

    public void setCodFlu(String codFlu) {
        this.codFlu = codFlu;
    }

    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    public String getCodRenta() {
        return codRenta;
    }

    public void setCodRenta(String codRenta) {
        this.codRenta = codRenta;
    }

    public short getIndCl1() {
        return indCl1;
    }

    public void setIndCl1(short indCl1) {
        this.indCl1 = indCl1;
    }

    public short getIndCl2() {
        return indCl2;
    }

    public void setIndCl2(short indCl2) {
        this.indCl2 = indCl2;
    }

    public short getIndCl3() {
        return indCl3;
    }

    public void setIndCl3(short indCl3) {
        this.indCl3 = indCl3;
    }

    public Short getDigChe() {
        return digChe;
    }

    public void setDigChe(Short digChe) {
        this.digChe = digChe;
    }

    public Short getEstCta() {
        return estCta;
    }

    public void setEstCta(Short estCta) {
        this.estCta = estCta;
    }

    public String getCodAgr() {
        return codAgr;
    }

    public void setCodAgr(String codAgr) {
        this.codAgr = codAgr;
    }

    public String getCtaMat() {
        return ctaMat;
    }

    public void setCtaMat(String ctaMat) {
        this.ctaMat = ctaMat;
    }

    public String getNomMat() {
        return nomMat;
    }

    public void setNomMat(String nomMat) {
        this.nomMat = nomMat;
    }

    public Short getIndAdc() {
        return indAdc;
    }

    public void setIndAdc(Short indAdc) {
        this.indAdc = indAdc;
    }

    public String getCtaIdc() {
        return ctaIdc;
    }

    public void setCtaIdc(String ctaIdc) {
        this.ctaIdc = ctaIdc;
    }

    public String getCtaGdc() {
        return ctaGdc;
    }

    public void setCtaGdc(String ctaGdc) {
        this.ctaGdc = ctaGdc;
    }

    public Short getIndTas() {
        return indTas;
    }

    public void setIndTas(Short indTas) {
        this.indTas = indTas;
    }

    public Short getIndBal() {
        return indBal;
    }

    public void setIndBal(Short indBal) {
        this.indBal = indBal;
    }

    public Short getIndDif() {
        return indDif;
    }

    public void setIndDif(Short indDif) {
        this.indDif = indDif;
    }

    public Short getIndCor() {
        return indCor;
    }

    public void setIndCor(Short indCor) {
        this.indCor = indCor;
    }

    public GenMonedas getTipMon() {
        return tipMon;
    }

    public void setTipMon(GenMonedas tipMon) {
        this.tipMon = tipMon;
    }

    public List<CntCuedoc> getCntCuedocList() {
        return cntCuedocList;
    }

    public void setCntCuedocList(List<CntCuedoc> cntCuedocList) {
        this.cntCuedocList = cntCuedocList;
    }

    public List<GenSucursal> getGenSucursalList() {
        return genSucursalList;
    }

    public void setGenSucursalList(List<GenSucursal> genSucursalList) {
        this.genSucursalList = genSucursalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCta != null ? codCta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CntPuc)) {
            return false;
        }
        CntPuc other = (CntPuc) object;
        if ((this.codCta == null && other.codCta != null) || (this.codCta != null && !this.codCta.equals(other.codCta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.CntPuc[ codCta=" + codCta + " ]";
    }
    
}
