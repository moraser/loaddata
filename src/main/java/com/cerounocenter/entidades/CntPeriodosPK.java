/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Embeddable
public class CntPeriodosPK implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "cod_ano", nullable = false, length = 4)
    private String codAno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "cod_per", nullable = false, length = 2)
    private String codPer;

    public CntPeriodosPK() {
    }

    public CntPeriodosPK(String codAno, String codPer) {
        this.codAno = codAno;
        this.codPer = codPer;
    }

    public String getCodAno() {
        return codAno;
    }

    public void setCodAno(String codAno) {
        this.codAno = codAno;
    }

    public String getCodPer() {
        return codPer;
    }

    public void setCodPer(String codPer) {
        this.codPer = codPer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codAno != null ? codAno.hashCode() : 0);
        hash += (codPer != null ? codPer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CntPeriodosPK)) {
            return false;
        }
        CntPeriodosPK other = (CntPeriodosPK) object;
        if ((this.codAno == null && other.codAno != null) || (this.codAno != null && !this.codAno.equals(other.codAno))) {
            return false;
        }
        if ((this.codPer == null && other.codPer != null) || (this.codPer != null && !this.codPer.equals(other.codPer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.CntPeriodosPK[ codAno=" + codAno + ", codPer=" + codPer + " ]";
    }
    
}
