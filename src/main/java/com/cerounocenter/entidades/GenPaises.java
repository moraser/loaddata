/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_paises", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenPaises.findAll", query = "SELECT g FROM GenPaises g")
    , @NamedQuery(name = "GenPaises.findByCodPai", query = "SELECT g FROM GenPaises g WHERE g.codPai = :codPai")
    , @NamedQuery(name = "GenPaises.findByNomPai", query = "SELECT g FROM GenPaises g WHERE g.nomPai = :nomPai")
    , @NamedQuery(name = "GenPaises.findByIndTel", query = "SELECT g FROM GenPaises g WHERE g.indTel = :indTel")
    , @NamedQuery(name = "GenPaises.findByCodDian", query = "SELECT g FROM GenPaises g WHERE g.codDian = :codDian")})
public class GenPaises implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "cod_pai", nullable = false, length = 3)
    private String codPai;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nom_pai", nullable = false, length = 30)
    private String nomPai;
    @Size(max = 3)
    @Column(name = "ind_tel", length = 3)
    private String indTel;
    @Size(max = 3)
    @Column(name = "cod_dian", length = 3)
    private String codDian;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genPaises")
    private List<GenCiudad> genCiudadList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genPaises")
    private List<GenDeptos> genDeptosList;

    public GenPaises() {
    }

    public GenPaises(String codPai) {
        this.codPai = codPai;
    }

    public GenPaises(String codPai, String nomPai) {
        this.codPai = codPai;
        this.nomPai = nomPai;
    }

    public String getCodPai() {
        return codPai;
    }

    public void setCodPai(String codPai) {
        this.codPai = codPai;
    }

    public String getNomPai() {
        return nomPai;
    }

    public void setNomPai(String nomPai) {
        this.nomPai = nomPai;
    }

    public String getIndTel() {
        return indTel;
    }

    public void setIndTel(String indTel) {
        this.indTel = indTel;
    }

    public String getCodDian() {
        return codDian;
    }

    public void setCodDian(String codDian) {
        this.codDian = codDian;
    }

    public List<GenCiudad> getGenCiudadList() {
        return genCiudadList;
    }

    public void setGenCiudadList(List<GenCiudad> genCiudadList) {
        this.genCiudadList = genCiudadList;
    }

    public List<GenDeptos> getGenDeptosList() {
        return genDeptosList;
    }

    public void setGenDeptosList(List<GenDeptos> genDeptosList) {
        this.genDeptosList = genDeptosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPai != null ? codPai.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenPaises)) {
            return false;
        }
        GenPaises other = (GenPaises) object;
        if ((this.codPai == null && other.codPai != null) || (this.codPai != null && !this.codPai.equals(other.codPai))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenPaises[ codPai=" + codPai + " ]";
    }
    
}
