/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "cnt_cabdoc", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "CntCabdoc.findAll", query = "SELECT c FROM CntCabdoc c")
    , @NamedQuery(name = "CntCabdoc.findByAnoDoc", query = "SELECT c FROM CntCabdoc c WHERE c.cntCabdocPK.anoDoc = :anoDoc")
    , @NamedQuery(name = "CntCabdoc.findByPerDoc", query = "SELECT c FROM CntCabdoc c WHERE c.cntCabdocPK.perDoc = :perDoc")
    , @NamedQuery(name = "CntCabdoc.findByTipDoc", query = "SELECT c FROM CntCabdoc c WHERE c.cntCabdocPK.tipDoc = :tipDoc")
    , @NamedQuery(name = "CntCabdoc.findByNumDoc", query = "SELECT c FROM CntCabdoc c WHERE c.cntCabdocPK.numDoc = :numDoc")
    , @NamedQuery(name = "CntCabdoc.findByFchDoc", query = "SELECT c FROM CntCabdoc c WHERE c.fchDoc = :fchDoc")
    , @NamedQuery(name = "CntCabdoc.findByCambio", query = "SELECT c FROM CntCabdoc c WHERE c.cambio = :cambio")
    , @NamedQuery(name = "CntCabdoc.findByTotDeb", query = "SELECT c FROM CntCabdoc c WHERE c.totDeb = :totDeb")
    , @NamedQuery(name = "CntCabdoc.findByTotCre", query = "SELECT c FROM CntCabdoc c WHERE c.totCre = :totCre")
    , @NamedQuery(name = "CntCabdoc.findByTotDif", query = "SELECT c FROM CntCabdoc c WHERE c.totDif = :totDif")
    , @NamedQuery(name = "CntCabdoc.findByAplOri", query = "SELECT c FROM CntCabdoc c WHERE c.aplOri = :aplOri")
    , @NamedQuery(name = "CntCabdoc.findByGrupo", query = "SELECT c FROM CntCabdoc c WHERE c.grupo = :grupo")
    , @NamedQuery(name = "CntCabdoc.findByUsuario", query = "SELECT c FROM CntCabdoc c WHERE c.usuario = :usuario")
    , @NamedQuery(name = "CntCabdoc.findByValBas", query = "SELECT c FROM CntCabdoc c WHERE c.valBas = :valBas")
    , @NamedQuery(name = "CntCabdoc.findByCodPlan", query = "SELECT c FROM CntCabdoc c WHERE c.codPlan = :codPlan")
    , @NamedQuery(name = "CntCabdoc.findByDetDoc", query = "SELECT c FROM CntCabdoc c WHERE c.detDoc = :detDoc")
    , @NamedQuery(name = "CntCabdoc.findByIndTas", query = "SELECT c FROM CntCabdoc c WHERE c.indTas = :indTas")
    , @NamedQuery(name = "CntCabdoc.findByFecGrab", query = "SELECT c FROM CntCabdoc c WHERE c.fecGrab = :fecGrab")
    , @NamedQuery(name = "CntCabdoc.findBySubTip", query = "SELECT c FROM CntCabdoc c WHERE c.subTip = :subTip")})
public class CntCabdoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CntCabdocPK cntCabdocPK;
    @Column(name = "fch_doc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fchDoc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cambio", nullable = false)
    private Character cambio;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "tot_deb", precision = 19, scale = 4)
    private BigDecimal totDeb;
    @Column(name = "tot_cre", precision = 19, scale = 4)
    private BigDecimal totCre;
    @Column(name = "tot_dif", precision = 19, scale = 4)
    private BigDecimal totDif;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "apl_ori", nullable = false, length = 3)
    private String aplOri;
    @Size(max = 35)
    @Column(name = "grupo", length = 35)
    private String grupo;
    @Size(max = 50)
    @Column(name = "usuario", length = 50)
    private String usuario;
    @Column(name = "val_bas", precision = 19, scale = 4)
    private BigDecimal valBas;
    @Size(max = 4)
    @Column(name = "cod_plan", length = 4)
    private String codPlan;
    @Size(max = 40)
    @Column(name = "det_doc", length = 40)
    private String detDoc;
    @Column(name = "ind_tas")
    private Character indTas;
    @Column(name = "fec_grab")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecGrab;
    @Size(max = 5)
    @Column(name = "sub_tip", length = 5)
    private String subTip;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cntCabdoc")
    private List<CntCuedoc> cntCuedocList;
    @JoinColumns({
        @JoinColumn(name = "ano_doc", referencedColumnName = "cod_ano", nullable = false, insertable = false, updatable = false)
        , @JoinColumn(name = "per_doc", referencedColumnName = "cod_per", nullable = false, insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CntPeriodos cntPeriodos;
    @JoinColumn(name = "tip_doc", referencedColumnName = "cod_tip", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CntTipodoc cntTipodoc;
    @JoinColumn(name = "ind_mp", referencedColumnName = "cod_mon")
    @ManyToOne
    private GenMonedas indMp;

    public CntCabdoc() {
    }

    public CntCabdoc(CntCabdocPK cntCabdocPK) {
        this.cntCabdocPK = cntCabdocPK;
    }

    public CntCabdoc(CntCabdocPK cntCabdocPK, Character cambio, String aplOri) {
        this.cntCabdocPK = cntCabdocPK;
        this.cambio = cambio;
        this.aplOri = aplOri;
    }

    public CntCabdoc(String anoDoc, String perDoc, String tipDoc, String numDoc) {
        this.cntCabdocPK = new CntCabdocPK(anoDoc, perDoc, tipDoc, numDoc);
    }

    public CntCabdocPK getCntCabdocPK() {
        return cntCabdocPK;
    }

    public void setCntCabdocPK(CntCabdocPK cntCabdocPK) {
        this.cntCabdocPK = cntCabdocPK;
    }

    public Date getFchDoc() {
        return fchDoc;
    }

    public void setFchDoc(Date fchDoc) {
        this.fchDoc = fchDoc;
    }

    public Character getCambio() {
        return cambio;
    }

    public void setCambio(Character cambio) {
        this.cambio = cambio;
    }

    public BigDecimal getTotDeb() {
        return totDeb;
    }

    public void setTotDeb(BigDecimal totDeb) {
        this.totDeb = totDeb;
    }

    public BigDecimal getTotCre() {
        return totCre;
    }

    public void setTotCre(BigDecimal totCre) {
        this.totCre = totCre;
    }

    public BigDecimal getTotDif() {
        return totDif;
    }

    public void setTotDif(BigDecimal totDif) {
        this.totDif = totDif;
    }

    public String getAplOri() {
        return aplOri;
    }

    public void setAplOri(String aplOri) {
        this.aplOri = aplOri;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public BigDecimal getValBas() {
        return valBas;
    }

    public void setValBas(BigDecimal valBas) {
        this.valBas = valBas;
    }

    public String getCodPlan() {
        return codPlan;
    }

    public void setCodPlan(String codPlan) {
        this.codPlan = codPlan;
    }

    public String getDetDoc() {
        return detDoc;
    }

    public void setDetDoc(String detDoc) {
        this.detDoc = detDoc;
    }

    public Character getIndTas() {
        return indTas;
    }

    public void setIndTas(Character indTas) {
        this.indTas = indTas;
    }

    public Date getFecGrab() {
        return fecGrab;
    }

    public void setFecGrab(Date fecGrab) {
        this.fecGrab = fecGrab;
    }

    public String getSubTip() {
        return subTip;
    }

    public void setSubTip(String subTip) {
        this.subTip = subTip;
    }

    public List<CntCuedoc> getCntCuedocList() {
        return cntCuedocList;
    }

    public void setCntCuedocList(List<CntCuedoc> cntCuedocList) {
        this.cntCuedocList = cntCuedocList;
    }

    public CntPeriodos getCntPeriodos() {
        return cntPeriodos;
    }

    public void setCntPeriodos(CntPeriodos cntPeriodos) {
        this.cntPeriodos = cntPeriodos;
    }

    public CntTipodoc getCntTipodoc() {
        return cntTipodoc;
    }

    public void setCntTipodoc(CntTipodoc cntTipodoc) {
        this.cntTipodoc = cntTipodoc;
    }

    public GenMonedas getIndMp() {
        return indMp;
    }

    public void setIndMp(GenMonedas indMp) {
        this.indMp = indMp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cntCabdocPK != null ? cntCabdocPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CntCabdoc)) {
            return false;
        }
        CntCabdoc other = (CntCabdoc) object;
        if ((this.cntCabdocPK == null && other.cntCabdocPK != null) || (this.cntCabdocPK != null && !this.cntCabdocPK.equals(other.cntCabdocPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.CntCabdoc[ cntCabdocPK=" + cntCabdocPK + " ]";
    }
    
}
