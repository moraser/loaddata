/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_tipide", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenTipide.findAll", query = "SELECT g FROM GenTipide g")
    , @NamedQuery(name = "GenTipide.findByCodTip", query = "SELECT g FROM GenTipide g WHERE g.codTip = :codTip")
    , @NamedQuery(name = "GenTipide.findByDesTip", query = "SELECT g FROM GenTipide g WHERE g.desTip = :desTip")
    , @NamedQuery(name = "GenTipide.findByTipTip", query = "SELECT g FROM GenTipide g WHERE g.tipTip = :tipTip")
    , @NamedQuery(name = "GenTipide.findByCodDian", query = "SELECT g FROM GenTipide g WHERE g.codDian = :codDian")})
public class GenTipide implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "cod_tip", nullable = false, length = 2)
    private String codTip;
    @Size(max = 40)
    @Column(name = "des_tip", length = 40)
    private String desTip;
    @Size(max = 2)
    @Column(name = "tip_tip", length = 2)
    private String tipTip;
    @Size(max = 2)
    @Column(name = "cod_dian", length = 2)
    private String codDian;
    @OneToMany(mappedBy = "tipIde")
    private List<GenTerceros> genTercerosList;

    public GenTipide() {
    }

    public GenTipide(String codTip) {
        this.codTip = codTip;
    }

    public String getCodTip() {
        return codTip;
    }

    public void setCodTip(String codTip) {
        this.codTip = codTip;
    }

    public String getDesTip() {
        return desTip;
    }

    public void setDesTip(String desTip) {
        this.desTip = desTip;
    }

    public String getTipTip() {
        return tipTip;
    }

    public void setTipTip(String tipTip) {
        this.tipTip = tipTip;
    }

    public String getCodDian() {
        return codDian;
    }

    public void setCodDian(String codDian) {
        this.codDian = codDian;
    }

    public List<GenTerceros> getGenTercerosList() {
        return genTercerosList;
    }

    public void setGenTercerosList(List<GenTerceros> genTercerosList) {
        this.genTercerosList = genTercerosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTip != null ? codTip.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenTipide)) {
            return false;
        }
        GenTipide other = (GenTipide) object;
        if ((this.codTip == null && other.codTip != null) || (this.codTip != null && !this.codTip.equals(other.codTip))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenTipide[ codTip=" + codTip + " ]";
    }
    
}
