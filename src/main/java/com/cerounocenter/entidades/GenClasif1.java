/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "gen_clasif1", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "GenClasif1.findAll", query = "SELECT g FROM GenClasif1 g")
    , @NamedQuery(name = "GenClasif1.findByCodigo", query = "SELECT g FROM GenClasif1 g WHERE g.codigo = :codigo")
    , @NamedQuery(name = "GenClasif1.findByNombre", query = "SELECT g FROM GenClasif1 g WHERE g.nombre = :nombre")
    , @NamedQuery(name = "GenClasif1.findByEstado", query = "SELECT g FROM GenClasif1 g WHERE g.estado = :estado")})
public class GenClasif1 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "codigo", nullable = false, length = 12)
    private String codigo;
    @Size(max = 40)
    @Column(name = "nombre", length = 40)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado", nullable = false)
    private short estado;
    @OneToMany(mappedBy = "codCl1")
    private List<CntCuedoc> cntCuedocList;

    public GenClasif1() {
    }

    public GenClasif1(String codigo) {
        this.codigo = codigo;
    }

    public GenClasif1(String codigo, short estado) {
        this.codigo = codigo;
        this.estado = estado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public List<CntCuedoc> getCntCuedocList() {
        return cntCuedocList;
    }

    public void setCntCuedocList(List<CntCuedoc> cntCuedocList) {
        this.cntCuedocList = cntCuedocList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenClasif1)) {
            return false;
        }
        GenClasif1 other = (GenClasif1) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.GenClasif1[ codigo=" + codigo + " ]";
    }
    
}
