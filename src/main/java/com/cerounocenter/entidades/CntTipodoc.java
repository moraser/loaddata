/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerounocenter.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sysadmin
 */
@Entity
@Table(name = "cnt_tipodoc", catalog = "HPH_COPIA", schema = "dbo")
@NamedQueries({
    @NamedQuery(name = "CntTipodoc.findAll", query = "SELECT c FROM CntTipodoc c")
    , @NamedQuery(name = "CntTipodoc.findByCodTip", query = "SELECT c FROM CntTipodoc c WHERE c.codTip = :codTip")
    , @NamedQuery(name = "CntTipodoc.findByNomTip", query = "SELECT c FROM CntTipodoc c WHERE c.nomTip = :nomTip")
    , @NamedQuery(name = "CntTipodoc.findByAbrTip", query = "SELECT c FROM CntTipodoc c WHERE c.abrTip = :abrTip")
    , @NamedQuery(name = "CntTipodoc.findByNumAut", query = "SELECT c FROM CntTipodoc c WHERE c.numAut = :numAut")
    , @NamedQuery(name = "CntTipodoc.findByNumIni", query = "SELECT c FROM CntTipodoc c WHERE c.numIni = :numIni")
    , @NamedQuery(name = "CntTipodoc.findByNumAct", query = "SELECT c FROM CntTipodoc c WHERE c.numAct = :numAct")
    , @NamedQuery(name = "CntTipodoc.findByLonNum", query = "SELECT c FROM CntTipodoc c WHERE c.lonNum = :lonNum")
    , @NamedQuery(name = "CntTipodoc.findByCarNum", query = "SELECT c FROM CntTipodoc c WHERE c.carNum = :carNum")
    , @NamedQuery(name = "CntTipodoc.findByCamCab", query = "SELECT c FROM CntTipodoc c WHERE c.camCab = :camCab")
    , @NamedQuery(name = "CntTipodoc.findByCamCue", query = "SELECT c FROM CntTipodoc c WHERE c.camCue = :camCue")
    , @NamedQuery(name = "CntTipodoc.findByCamTot", query = "SELECT c FROM CntTipodoc c WHERE c.camTot = :camTot")
    , @NamedQuery(name = "CntTipodoc.findByTrnReg", query = "SELECT c FROM CntTipodoc c WHERE c.trnReg = :trnReg")
    , @NamedQuery(name = "CntTipodoc.findByTraReg", query = "SELECT c FROM CntTipodoc c WHERE c.traReg = :traReg")
    , @NamedQuery(name = "CntTipodoc.findByTraImp", query = "SELECT c FROM CntTipodoc c WHERE c.traImp = :traImp")
    , @NamedQuery(name = "CntTipodoc.findByTraFin", query = "SELECT c FROM CntTipodoc c WHERE c.traFin = :traFin")
    , @NamedQuery(name = "CntTipodoc.findByTraAnu", query = "SELECT c FROM CntTipodoc c WHERE c.traAnu = :traAnu")
    , @NamedQuery(name = "CntTipodoc.findByTraMod", query = "SELECT c FROM CntTipodoc c WHERE c.traMod = :traMod")
    , @NamedQuery(name = "CntTipodoc.findByTraCre", query = "SELECT c FROM CntTipodoc c WHERE c.traCre = :traCre")
    , @NamedQuery(name = "CntTipodoc.findByTraFca", query = "SELECT c FROM CntTipodoc c WHERE c.traFca = :traFca")
    , @NamedQuery(name = "CntTipodoc.findByNatDoc", query = "SELECT c FROM CntTipodoc c WHERE c.natDoc = :natDoc")
    , @NamedQuery(name = "CntTipodoc.findByModAnu", query = "SELECT c FROM CntTipodoc c WHERE c.modAnu = :modAnu")
    , @NamedQuery(name = "CntTipodoc.findByAnoTip", query = "SELECT c FROM CntTipodoc c WHERE c.anoTip = :anoTip")
    , @NamedQuery(name = "CntTipodoc.findByPerTip", query = "SELECT c FROM CntTipodoc c WHERE c.perTip = :perTip")
    , @NamedQuery(name = "CntTipodoc.findByTipCon", query = "SELECT c FROM CntTipodoc c WHERE c.tipCon = :tipCon")
    , @NamedQuery(name = "CntTipodoc.findByIndNum", query = "SELECT c FROM CntTipodoc c WHERE c.indNum = :indNum")
    , @NamedQuery(name = "CntTipodoc.findByTraCon", query = "SELECT c FROM CntTipodoc c WHERE c.traCon = :traCon")
    , @NamedQuery(name = "CntTipodoc.findByTraRefresco", query = "SELECT c FROM CntTipodoc c WHERE c.traRefresco = :traRefresco")
    , @NamedQuery(name = "CntTipodoc.findByTraValcuerpo", query = "SELECT c FROM CntTipodoc c WHERE c.traValcuerpo = :traValcuerpo")
    , @NamedQuery(name = "CntTipodoc.findByTraInicuerpo", query = "SELECT c FROM CntTipodoc c WHERE c.traInicuerpo = :traInicuerpo")
    , @NamedQuery(name = "CntTipodoc.findByCtaAlo", query = "SELECT c FROM CntTipodoc c WHERE c.ctaAlo = :ctaAlo")
    , @NamedQuery(name = "CntTipodoc.findByCtaAex", query = "SELECT c FROM CntTipodoc c WHERE c.ctaAex = :ctaAex")
    , @NamedQuery(name = "CntTipodoc.findBySucAdc", query = "SELECT c FROM CntTipodoc c WHERE c.sucAdc = :sucAdc")
    , @NamedQuery(name = "CntTipodoc.findByCcoAdc", query = "SELECT c FROM CntTipodoc c WHERE c.ccoAdc = :ccoAdc")
    , @NamedQuery(name = "CntTipodoc.findByTerAdc", query = "SELECT c FROM CntTipodoc c WHERE c.terAdc = :terAdc")
    , @NamedQuery(name = "CntTipodoc.findByCl1Adc", query = "SELECT c FROM CntTipodoc c WHERE c.cl1Adc = :cl1Adc")
    , @NamedQuery(name = "CntTipodoc.findByCl2Adc", query = "SELECT c FROM CntTipodoc c WHERE c.cl2Adc = :cl2Adc")
    , @NamedQuery(name = "CntTipodoc.findByCl3Adc", query = "SELECT c FROM CntTipodoc c WHERE c.cl3Adc = :cl3Adc")
    , @NamedQuery(name = "CntTipodoc.findByCamPie", query = "SELECT c FROM CntTipodoc c WHERE c.camPie = :camPie")
    , @NamedQuery(name = "CntTipodoc.findByTraFincuerpo", query = "SELECT c FROM CntTipodoc c WHERE c.traFincuerpo = :traFincuerpo")
    , @NamedQuery(name = "CntTipodoc.findByTraCan", query = "SELECT c FROM CntTipodoc c WHERE c.traCan = :traCan")
    , @NamedQuery(name = "CntTipodoc.findByCtaAlog", query = "SELECT c FROM CntTipodoc c WHERE c.ctaAlog = :ctaAlog")
    , @NamedQuery(name = "CntTipodoc.findByCtaAexg", query = "SELECT c FROM CntTipodoc c WHERE c.ctaAexg = :ctaAexg")
    , @NamedQuery(name = "CntTipodoc.findByIndAju", query = "SELECT c FROM CntTipodoc c WHERE c.indAju = :indAju")
    , @NamedQuery(name = "CntTipodoc.findByMaxAju", query = "SELECT c FROM CntTipodoc c WHERE c.maxAju = :maxAju")
    , @NamedQuery(name = "CntTipodoc.findByAjuDeb", query = "SELECT c FROM CntTipodoc c WHERE c.ajuDeb = :ajuDeb")
    , @NamedQuery(name = "CntTipodoc.findByAjuCre", query = "SELECT c FROM CntTipodoc c WHERE c.ajuCre = :ajuCre")
    , @NamedQuery(name = "CntTipodoc.findByAjuSuc", query = "SELECT c FROM CntTipodoc c WHERE c.ajuSuc = :ajuSuc")
    , @NamedQuery(name = "CntTipodoc.findByAjuCco", query = "SELECT c FROM CntTipodoc c WHERE c.ajuCco = :ajuCco")
    , @NamedQuery(name = "CntTipodoc.findByAjuCl1", query = "SELECT c FROM CntTipodoc c WHERE c.ajuCl1 = :ajuCl1")
    , @NamedQuery(name = "CntTipodoc.findByAjuCl2", query = "SELECT c FROM CntTipodoc c WHERE c.ajuCl2 = :ajuCl2")
    , @NamedQuery(name = "CntTipodoc.findByAjuCl3", query = "SELECT c FROM CntTipodoc c WHERE c.ajuCl3 = :ajuCl3")
    , @NamedQuery(name = "CntTipodoc.findByAjuTer", query = "SELECT c FROM CntTipodoc c WHERE c.ajuTer = :ajuTer")
    , @NamedQuery(name = "CntTipodoc.findByCamInfo", query = "SELECT c FROM CntTipodoc c WHERE c.camInfo = :camInfo")
    , @NamedQuery(name = "CntTipodoc.findByCamInfo1", query = "SELECT c FROM CntTipodoc c WHERE c.camInfo1 = :camInfo1")
    , @NamedQuery(name = "CntTipodoc.findByCamInfo2", query = "SELECT c FROM CntTipodoc c WHERE c.camInfo2 = :camInfo2")
    , @NamedQuery(name = "CntTipodoc.findByCamInfo3", query = "SELECT c FROM CntTipodoc c WHERE c.camInfo3 = :camInfo3")
    , @NamedQuery(name = "CntTipodoc.findByCamInfo4", query = "SELECT c FROM CntTipodoc c WHERE c.camInfo4 = :camInfo4")
    , @NamedQuery(name = "CntTipodoc.findByCamInfo5", query = "SELECT c FROM CntTipodoc c WHERE c.camInfo5 = :camInfo5")
    , @NamedQuery(name = "CntTipodoc.findByCamInfo6", query = "SELECT c FROM CntTipodoc c WHERE c.camInfo6 = :camInfo6")
    , @NamedQuery(name = "CntTipodoc.findByCamInfo7", query = "SELECT c FROM CntTipodoc c WHERE c.camInfo7 = :camInfo7")
    , @NamedQuery(name = "CntTipodoc.findByMaeInfo1", query = "SELECT c FROM CntTipodoc c WHERE c.maeInfo1 = :maeInfo1")
    , @NamedQuery(name = "CntTipodoc.findByMaeInfo2", query = "SELECT c FROM CntTipodoc c WHERE c.maeInfo2 = :maeInfo2")
    , @NamedQuery(name = "CntTipodoc.findByMaeInfo3", query = "SELECT c FROM CntTipodoc c WHERE c.maeInfo3 = :maeInfo3")
    , @NamedQuery(name = "CntTipodoc.findByMaeInfo6", query = "SELECT c FROM CntTipodoc c WHERE c.maeInfo6 = :maeInfo6")
    , @NamedQuery(name = "CntTipodoc.findByMaeInfo7", query = "SELECT c FROM CntTipodoc c WHERE c.maeInfo7 = :maeInfo7")
    , @NamedQuery(name = "CntTipodoc.findByMaeInfo4", query = "SELECT c FROM CntTipodoc c WHERE c.maeInfo4 = :maeInfo4")
    , @NamedQuery(name = "CntTipodoc.findByMaeInfo5", query = "SELECT c FROM CntTipodoc c WHERE c.maeInfo5 = :maeInfo5")})
public class CntTipodoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "cod_tip", nullable = false, length = 3)
    private String codTip;
    @Size(max = 30)
    @Column(name = "nom_tip", length = 30)
    private String nomTip;
    @Size(max = 5)
    @Column(name = "abr_tip", length = 5)
    private String abrTip;
    @Basic(optional = false)
    @NotNull
    @Column(name = "num_aut", nullable = false)
    private boolean numAut;
    @Column(name = "num_ini")
    private Short numIni;
    @Column(name = "num_act")
    private Integer numAct;
    @Column(name = "lon_num")
    private Integer lonNum;
    @Size(max = 5)
    @Column(name = "car_num", length = 5)
    private String carNum;
    @Size(max = 120)
    @Column(name = "cam_cab", length = 120)
    private String camCab;
    @Size(max = 120)
    @Column(name = "cam_cue", length = 120)
    private String camCue;
    @Size(max = 50)
    @Column(name = "cam_tot", length = 50)
    private String camTot;
    @Size(max = 5)
    @Column(name = "trn_reg", length = 5)
    private String trnReg;
    @Size(max = 5)
    @Column(name = "tra_reg", length = 5)
    private String traReg;
    @Size(max = 5)
    @Column(name = "tra_imp", length = 5)
    private String traImp;
    @Size(max = 5)
    @Column(name = "tra_fin", length = 5)
    private String traFin;
    @Size(max = 5)
    @Column(name = "tra_anu", length = 5)
    private String traAnu;
    @Size(max = 5)
    @Column(name = "tra_mod", length = 5)
    private String traMod;
    @Size(max = 5)
    @Column(name = "tra_cre", length = 5)
    private String traCre;
    @Size(max = 5)
    @Column(name = "tra_fca", length = 5)
    private String traFca;
    @Column(name = "nat_doc")
    private Character natDoc;
    @Column(name = "mod_anu")
    private Short modAnu;
    @Size(max = 4)
    @Column(name = "ano_tip", length = 4)
    private String anoTip;
    @Size(max = 2)
    @Column(name = "per_tip", length = 2)
    private String perTip;
    @Size(max = 3)
    @Column(name = "tip_con", length = 3)
    private String tipCon;
    @Column(name = "ind_num")
    private Short indNum;
    @Size(max = 250)
    @Column(name = "tra_con", length = 250)
    private String traCon;
    @Size(max = 250)
    @Column(name = "tra_refresco", length = 250)
    private String traRefresco;
    @Size(max = 250)
    @Column(name = "tra_valcuerpo", length = 250)
    private String traValcuerpo;
    @Size(max = 250)
    @Column(name = "tra_inicuerpo", length = 250)
    private String traInicuerpo;
    @Size(max = 16)
    @Column(name = "cta_alo", length = 16)
    private String ctaAlo;
    @Size(max = 16)
    @Column(name = "cta_aex", length = 16)
    private String ctaAex;
    @Size(max = 3)
    @Column(name = "suc_adc", length = 3)
    private String sucAdc;
    @Size(max = 10)
    @Column(name = "cco_adc", length = 10)
    private String ccoAdc;
    @Size(max = 15)
    @Column(name = "ter_adc", length = 15)
    private String terAdc;
    @Size(max = 12)
    @Column(name = "cl1_adc", length = 12)
    private String cl1Adc;
    @Size(max = 12)
    @Column(name = "cl2_adc", length = 12)
    private String cl2Adc;
    @Size(max = 12)
    @Column(name = "cl3_adc", length = 12)
    private String cl3Adc;
    @Size(max = 50)
    @Column(name = "cam_pie", length = 50)
    private String camPie;
    @Size(max = 50)
    @Column(name = "tra_fincuerpo", length = 50)
    private String traFincuerpo;
    @Size(max = 250)
    @Column(name = "tra_can", length = 250)
    private String traCan;
    @Size(max = 16)
    @Column(name = "cta_alog", length = 16)
    private String ctaAlog;
    @Size(max = 16)
    @Column(name = "cta_aexg", length = 16)
    private String ctaAexg;
    @Column(name = "ind_aju")
    private Boolean indAju;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "max_aju", precision = 19, scale = 4)
    private BigDecimal maxAju;
    @Size(max = 16)
    @Column(name = "aju_deb", length = 16)
    private String ajuDeb;
    @Size(max = 16)
    @Column(name = "aju_cre", length = 16)
    private String ajuCre;
    @Size(max = 3)
    @Column(name = "aju_suc", length = 3)
    private String ajuSuc;
    @Size(max = 10)
    @Column(name = "aju_cco", length = 10)
    private String ajuCco;
    @Size(max = 12)
    @Column(name = "aju_cl1", length = 12)
    private String ajuCl1;
    @Size(max = 12)
    @Column(name = "aju_cl2", length = 12)
    private String ajuCl2;
    @Size(max = 12)
    @Column(name = "aju_cl3", length = 12)
    private String ajuCl3;
    @Size(max = 15)
    @Column(name = "aju_ter", length = 15)
    private String ajuTer;
    @Size(max = 30)
    @Column(name = "cam_info", length = 30)
    private String camInfo;
    @Size(max = 254)
    @Column(name = "cam_info1", length = 254)
    private String camInfo1;
    @Size(max = 254)
    @Column(name = "cam_info2", length = 254)
    private String camInfo2;
    @Size(max = 254)
    @Column(name = "cam_info3", length = 254)
    private String camInfo3;
    @Size(max = 254)
    @Column(name = "cam_info4", length = 254)
    private String camInfo4;
    @Size(max = 254)
    @Column(name = "cam_info5", length = 254)
    private String camInfo5;
    @Size(max = 254)
    @Column(name = "cam_info6", length = 254)
    private String camInfo6;
    @Size(max = 254)
    @Column(name = "cam_info7", length = 254)
    private String camInfo7;
    @Column(name = "mae_info1")
    private Short maeInfo1;
    @Column(name = "mae_info2")
    private Short maeInfo2;
    @Column(name = "mae_info3")
    private Short maeInfo3;
    @Column(name = "mae_info6")
    private Short maeInfo6;
    @Column(name = "mae_info7")
    private Short maeInfo7;
    @Column(name = "mae_info4")
    private Short maeInfo4;
    @Column(name = "mae_info5")
    private Short maeInfo5;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cntTipodoc")
    private List<CntCuedoc> cntCuedocList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cntTipodoc")
    private List<CntCabdoc> cntCabdocList;

    public CntTipodoc() {
    }

    public CntTipodoc(String codTip) {
        this.codTip = codTip;
    }

    public CntTipodoc(String codTip, boolean numAut) {
        this.codTip = codTip;
        this.numAut = numAut;
    }

    public String getCodTip() {
        return codTip;
    }

    public void setCodTip(String codTip) {
        this.codTip = codTip;
    }

    public String getNomTip() {
        return nomTip;
    }

    public void setNomTip(String nomTip) {
        this.nomTip = nomTip;
    }

    public String getAbrTip() {
        return abrTip;
    }

    public void setAbrTip(String abrTip) {
        this.abrTip = abrTip;
    }

    public boolean getNumAut() {
        return numAut;
    }

    public void setNumAut(boolean numAut) {
        this.numAut = numAut;
    }

    public Short getNumIni() {
        return numIni;
    }

    public void setNumIni(Short numIni) {
        this.numIni = numIni;
    }

    public Integer getNumAct() {
        return numAct;
    }

    public void setNumAct(Integer numAct) {
        this.numAct = numAct;
    }

    public Integer getLonNum() {
        return lonNum;
    }

    public void setLonNum(Integer lonNum) {
        this.lonNum = lonNum;
    }

    public String getCarNum() {
        return carNum;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
    }

    public String getCamCab() {
        return camCab;
    }

    public void setCamCab(String camCab) {
        this.camCab = camCab;
    }

    public String getCamCue() {
        return camCue;
    }

    public void setCamCue(String camCue) {
        this.camCue = camCue;
    }

    public String getCamTot() {
        return camTot;
    }

    public void setCamTot(String camTot) {
        this.camTot = camTot;
    }

    public String getTrnReg() {
        return trnReg;
    }

    public void setTrnReg(String trnReg) {
        this.trnReg = trnReg;
    }

    public String getTraReg() {
        return traReg;
    }

    public void setTraReg(String traReg) {
        this.traReg = traReg;
    }

    public String getTraImp() {
        return traImp;
    }

    public void setTraImp(String traImp) {
        this.traImp = traImp;
    }

    public String getTraFin() {
        return traFin;
    }

    public void setTraFin(String traFin) {
        this.traFin = traFin;
    }

    public String getTraAnu() {
        return traAnu;
    }

    public void setTraAnu(String traAnu) {
        this.traAnu = traAnu;
    }

    public String getTraMod() {
        return traMod;
    }

    public void setTraMod(String traMod) {
        this.traMod = traMod;
    }

    public String getTraCre() {
        return traCre;
    }

    public void setTraCre(String traCre) {
        this.traCre = traCre;
    }

    public String getTraFca() {
        return traFca;
    }

    public void setTraFca(String traFca) {
        this.traFca = traFca;
    }

    public Character getNatDoc() {
        return natDoc;
    }

    public void setNatDoc(Character natDoc) {
        this.natDoc = natDoc;
    }

    public Short getModAnu() {
        return modAnu;
    }

    public void setModAnu(Short modAnu) {
        this.modAnu = modAnu;
    }

    public String getAnoTip() {
        return anoTip;
    }

    public void setAnoTip(String anoTip) {
        this.anoTip = anoTip;
    }

    public String getPerTip() {
        return perTip;
    }

    public void setPerTip(String perTip) {
        this.perTip = perTip;
    }

    public String getTipCon() {
        return tipCon;
    }

    public void setTipCon(String tipCon) {
        this.tipCon = tipCon;
    }

    public Short getIndNum() {
        return indNum;
    }

    public void setIndNum(Short indNum) {
        this.indNum = indNum;
    }

    public String getTraCon() {
        return traCon;
    }

    public void setTraCon(String traCon) {
        this.traCon = traCon;
    }

    public String getTraRefresco() {
        return traRefresco;
    }

    public void setTraRefresco(String traRefresco) {
        this.traRefresco = traRefresco;
    }

    public String getTraValcuerpo() {
        return traValcuerpo;
    }

    public void setTraValcuerpo(String traValcuerpo) {
        this.traValcuerpo = traValcuerpo;
    }

    public String getTraInicuerpo() {
        return traInicuerpo;
    }

    public void setTraInicuerpo(String traInicuerpo) {
        this.traInicuerpo = traInicuerpo;
    }

    public String getCtaAlo() {
        return ctaAlo;
    }

    public void setCtaAlo(String ctaAlo) {
        this.ctaAlo = ctaAlo;
    }

    public String getCtaAex() {
        return ctaAex;
    }

    public void setCtaAex(String ctaAex) {
        this.ctaAex = ctaAex;
    }

    public String getSucAdc() {
        return sucAdc;
    }

    public void setSucAdc(String sucAdc) {
        this.sucAdc = sucAdc;
    }

    public String getCcoAdc() {
        return ccoAdc;
    }

    public void setCcoAdc(String ccoAdc) {
        this.ccoAdc = ccoAdc;
    }

    public String getTerAdc() {
        return terAdc;
    }

    public void setTerAdc(String terAdc) {
        this.terAdc = terAdc;
    }

    public String getCl1Adc() {
        return cl1Adc;
    }

    public void setCl1Adc(String cl1Adc) {
        this.cl1Adc = cl1Adc;
    }

    public String getCl2Adc() {
        return cl2Adc;
    }

    public void setCl2Adc(String cl2Adc) {
        this.cl2Adc = cl2Adc;
    }

    public String getCl3Adc() {
        return cl3Adc;
    }

    public void setCl3Adc(String cl3Adc) {
        this.cl3Adc = cl3Adc;
    }

    public String getCamPie() {
        return camPie;
    }

    public void setCamPie(String camPie) {
        this.camPie = camPie;
    }

    public String getTraFincuerpo() {
        return traFincuerpo;
    }

    public void setTraFincuerpo(String traFincuerpo) {
        this.traFincuerpo = traFincuerpo;
    }

    public String getTraCan() {
        return traCan;
    }

    public void setTraCan(String traCan) {
        this.traCan = traCan;
    }

    public String getCtaAlog() {
        return ctaAlog;
    }

    public void setCtaAlog(String ctaAlog) {
        this.ctaAlog = ctaAlog;
    }

    public String getCtaAexg() {
        return ctaAexg;
    }

    public void setCtaAexg(String ctaAexg) {
        this.ctaAexg = ctaAexg;
    }

    public Boolean getIndAju() {
        return indAju;
    }

    public void setIndAju(Boolean indAju) {
        this.indAju = indAju;
    }

    public BigDecimal getMaxAju() {
        return maxAju;
    }

    public void setMaxAju(BigDecimal maxAju) {
        this.maxAju = maxAju;
    }

    public String getAjuDeb() {
        return ajuDeb;
    }

    public void setAjuDeb(String ajuDeb) {
        this.ajuDeb = ajuDeb;
    }

    public String getAjuCre() {
        return ajuCre;
    }

    public void setAjuCre(String ajuCre) {
        this.ajuCre = ajuCre;
    }

    public String getAjuSuc() {
        return ajuSuc;
    }

    public void setAjuSuc(String ajuSuc) {
        this.ajuSuc = ajuSuc;
    }

    public String getAjuCco() {
        return ajuCco;
    }

    public void setAjuCco(String ajuCco) {
        this.ajuCco = ajuCco;
    }

    public String getAjuCl1() {
        return ajuCl1;
    }

    public void setAjuCl1(String ajuCl1) {
        this.ajuCl1 = ajuCl1;
    }

    public String getAjuCl2() {
        return ajuCl2;
    }

    public void setAjuCl2(String ajuCl2) {
        this.ajuCl2 = ajuCl2;
    }

    public String getAjuCl3() {
        return ajuCl3;
    }

    public void setAjuCl3(String ajuCl3) {
        this.ajuCl3 = ajuCl3;
    }

    public String getAjuTer() {
        return ajuTer;
    }

    public void setAjuTer(String ajuTer) {
        this.ajuTer = ajuTer;
    }

    public String getCamInfo() {
        return camInfo;
    }

    public void setCamInfo(String camInfo) {
        this.camInfo = camInfo;
    }

    public String getCamInfo1() {
        return camInfo1;
    }

    public void setCamInfo1(String camInfo1) {
        this.camInfo1 = camInfo1;
    }

    public String getCamInfo2() {
        return camInfo2;
    }

    public void setCamInfo2(String camInfo2) {
        this.camInfo2 = camInfo2;
    }

    public String getCamInfo3() {
        return camInfo3;
    }

    public void setCamInfo3(String camInfo3) {
        this.camInfo3 = camInfo3;
    }

    public String getCamInfo4() {
        return camInfo4;
    }

    public void setCamInfo4(String camInfo4) {
        this.camInfo4 = camInfo4;
    }

    public String getCamInfo5() {
        return camInfo5;
    }

    public void setCamInfo5(String camInfo5) {
        this.camInfo5 = camInfo5;
    }

    public String getCamInfo6() {
        return camInfo6;
    }

    public void setCamInfo6(String camInfo6) {
        this.camInfo6 = camInfo6;
    }

    public String getCamInfo7() {
        return camInfo7;
    }

    public void setCamInfo7(String camInfo7) {
        this.camInfo7 = camInfo7;
    }

    public Short getMaeInfo1() {
        return maeInfo1;
    }

    public void setMaeInfo1(Short maeInfo1) {
        this.maeInfo1 = maeInfo1;
    }

    public Short getMaeInfo2() {
        return maeInfo2;
    }

    public void setMaeInfo2(Short maeInfo2) {
        this.maeInfo2 = maeInfo2;
    }

    public Short getMaeInfo3() {
        return maeInfo3;
    }

    public void setMaeInfo3(Short maeInfo3) {
        this.maeInfo3 = maeInfo3;
    }

    public Short getMaeInfo6() {
        return maeInfo6;
    }

    public void setMaeInfo6(Short maeInfo6) {
        this.maeInfo6 = maeInfo6;
    }

    public Short getMaeInfo7() {
        return maeInfo7;
    }

    public void setMaeInfo7(Short maeInfo7) {
        this.maeInfo7 = maeInfo7;
    }

    public Short getMaeInfo4() {
        return maeInfo4;
    }

    public void setMaeInfo4(Short maeInfo4) {
        this.maeInfo4 = maeInfo4;
    }

    public Short getMaeInfo5() {
        return maeInfo5;
    }

    public void setMaeInfo5(Short maeInfo5) {
        this.maeInfo5 = maeInfo5;
    }

    public List<CntCuedoc> getCntCuedocList() {
        return cntCuedocList;
    }

    public void setCntCuedocList(List<CntCuedoc> cntCuedocList) {
        this.cntCuedocList = cntCuedocList;
    }

    public List<CntCabdoc> getCntCabdocList() {
        return cntCabdocList;
    }

    public void setCntCabdocList(List<CntCabdoc> cntCabdocList) {
        this.cntCabdocList = cntCabdocList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTip != null ? codTip.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CntTipodoc)) {
            return false;
        }
        CntTipodoc other = (CntTipodoc) object;
        if ((this.codTip == null && other.codTip != null) || (this.codTip != null && !this.codTip.equals(other.codTip))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerounocenter.entidades.CntTipodoc[ codTip=" + codTip + " ]";
    }
    
}
